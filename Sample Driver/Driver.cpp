//	Change this path to your copy of libtiff
#include <libtiff/tiffiop.h>

#include <ADA/DriverInfo.h>
#include "Driver.h"

using namespace aur;

static ExceptionCode	Initialize( void* obj, ACPL::XML*, float dpiX, float dpiY, const char* settings )
{
	return ((CTIFFDriver*)obj)->Initialize( dpiX, dpiY, settings );
}

static void	Terminate( void* obj, bool abort )
{
	if( abort )
		((CTIFFDriver*)obj)->Abort();
}

static void	JobStart( void* obj, const ACPL::UString& jobName, int32_t&, const ADA::TMediaSize&, const ADA::TTray&, bool, bool )
{
	((CTIFFDriver*)obj)->JobStart( jobName );
}

static void	RasterStart( void* obj, int32_t, int32_t, int32_t width, int32_t height )
{
	((CTIFFDriver*)obj)->RasterStart( width, height );
}

static void	RasterEnd( void* obj )
{
	((CTIFFDriver*)obj)->RasterEnd();
}

static void	ProcessRawBuffer( void* obj, const uint8_t* planeData, int32_t dataBytes, uint8_t planeIdx )
{
	((CTIFFDriver*)obj)->ProcessRawBuffer( planeData, dataBytes, planeIdx );
}

static const ADA::DriverMethods	kMethods =
{
	Initialize,
	Terminate,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	JobStart,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	RasterStart,
	RasterEnd,
	ProcessRawBuffer,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr
};

void*	CreateSession( ADA::Instance* driverObj, const ADA::DriverMethods** method, const char* deviceURL )
{
	*method = &kMethods;
	return new CTIFFDriver( driverObj->mModelName, driverObj->mDeviceURL );
}

void	DisposeSession( ADA::Instance* /* driverObj */,void* cookie )
{
	delete (CTIFFDriver*)cookie;
}

CTIFFDriver::CTIFFDriver( const char* modelName, const char* deviceURI ) :
	mURI( deviceURI ),
	mLineBuffer( NULL ),
	mLibTif( NULL )
{
	mModel = strcmp( modelName, "Sample TIFF Contone" ) == 0 ? TIFF_CT : TIFF_HT;
	mLastPlane = mModel == TIFF_CT ? 3 : 0;
}

CTIFFDriver::~CTIFFDriver()
{
	delete[] mLineBuffer;
	if( mLibTif )
		TIFFClose( mLibTif );
}

ExceptionCode	CTIFFDriver::Initialize( float dpuX, float dpuY, const char* xmlSettings )
{
	mDPI_X = floorf( dpuX * 18.0f + 0.5f );
	mDPI_Y = floorf( dpuY * 18.0f + 0.5f );

	mFileName = L"Untitled";

	mCompression = COMPRESSION_NONE;
	const char* tag;
	if( ( tag = strstr( xmlSettings, "<Compression>" ) ) != NULL )
	{
		char	compressStr[32];
		sscanf( tag, "<Compression>%s", compressStr );
		if( strcmp( compressStr, "LZW" ) == 0 )
			mCompression = COMPRESSION_LZW;
		else if( strcmp( compressStr, "CCITT" ) == 0 )
			mCompression = COMPRESSION_CCITTFAX4;
	}

	ACPL::UString	path;
	if( mURI.Left(7) == "file://" )
		path = ACPL::UString::FromUtf8( &mURI[7] );
	else
		path = ACPL::UString::FromUtf8( mURI );
	return mFolderPath.Make( path );
}

void	CTIFFDriver::Abort()
{
	if( mLibTif )
	{
		TIFFClose( mLibTif );
		mLibTif = NULL;

		ACPL::FileSpec	tifSpec;
		if( tifSpec.Make( mFolderPath, mFileName + ".tif" ) == NoError )
			tifSpec.Delete();
	}
}

void	CTIFFDriver::JobStart( const ACPL::UString& name )
{
	mFileName = name;
}

void	CTIFFDriver::RasterStart( int32_t width, int32_t height )
{
	ACPL::FileSpec	tifSpec;
	tifSpec.Make( mFolderPath, mFileName + ".tif" );
	mLibTif = TIFFOpenW( tifSpec.GetPOSIXPath(), "w" );
	if( mLibTif )
	{
		TIFFSetField( mLibTif, TIFFTAG_IMAGEWIDTH, width );
		TIFFSetField( mLibTif, TIFFTAG_IMAGELENGTH, height );
		TIFFSetField( mLibTif, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
		TIFFSetField( mLibTif, TIFFTAG_XRESOLUTION, mDPI_X );
		TIFFSetField( mLibTif, TIFFTAG_YRESOLUTION, mDPI_Y );
		TIFFSetField( mLibTif, TIFFTAG_COMPRESSION, mCompression );
		if( mModel == TIFF_HT )
		{
			TIFFSetField( mLibTif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISWHITE );
			TIFFSetField( mLibTif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_SEPARATE );
			TIFFSetField( mLibTif, TIFFTAG_SAMPLESPERPIXEL, 1 );
			TIFFSetField( mLibTif, TIFFTAG_BITSPERSAMPLE, 1 );	//	1 bit halftone
			TIFFSetField( mLibTif, TIFFTAG_ROWSPERSTRIP, 128 );
		}
		else
		{
			TIFFSetField( mLibTif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_SEPARATED );
			TIFFSetField( mLibTif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG );
			TIFFSetField( mLibTif, TIFFTAG_ROWSPERSTRIP, 128 );
			TIFFSetField( mLibTif, TIFFTAG_SAMPLESPERPIXEL, 4 );	//	CMYK
			TIFFSetField( mLibTif, TIFFTAG_BITSPERSAMPLE, 8 );	//	8 bits per pixel
		}
	}

	mLine = 0;
	if( mModel == TIFF_HT )
		mLineSize = ( width + 7 ) / 8;
	else
		mLineSize = width * 4;
	mLineBuffer = new uint8_t[ mLineSize ];
	memset( mLineBuffer, 0, mLineSize );
}

void	CTIFFDriver::RasterEnd()
{
	TIFFClose( mLibTif );
	mLibTif = NULL;
}

void	CTIFFDriver::ProcessRawBuffer( const uint8_t* planeData, int32_t dataBytes, uint8_t planeIdx )
{
	if( mModel == TIFF_CT )	//	Interleave samples
	{
		const uint8_t* src = planeData;
		const uint8_t* srcEnd = planeData + dataBytes;
		uint8_t* dest = mLineBuffer + planeIdx;
		while( src != srcEnd )
		{
			*dest = *src++;
			dest += 4;
		}
	}
	else
		memcpy( mLineBuffer, planeData, dataBytes );		
	if( planeIdx == mLastPlane )
	{
		TIFFWriteScanline( mLibTif, mLineBuffer, mLine, 0 );
		++mLine;
		memset( mLineBuffer, 0, mLineSize );
	}
}
