#include "Driver.h"
#include <ACPL/XML.h>

using namespace aur;

const ADA::ModelID ADA::kDriverID[] =
{
	{ "Sample TIFF Contone", "TIFF", 0, 44 inch, 50000 mm },
	{ "Sample TIFF 1-bit", "TIFF", 0, 44 inch, 50000 mm },
	{ nullptr }
};

static const ADA::TInk	kInks[] =
{
	{ 55, -37, -49, "Cyan", ADA::eAMScreen, 0, 15, 43, ADA::eAdobeRound, 0.6f, ADA::eProcess, 2, { 1 }, { 0 } },
	{ 49, 72, -5, "Magenta", ADA::eAMScreen, 0, 30, 43, ADA::eAdobeRound, 0.6f, ADA::eProcess, 2, { 1 }, { 0 } },
	{ 89, -4, 90, "Yellow", ADA::eAMScreen, 0, 90, 53, ADA::eAdobeRound, 0.6f, ADA::eProcess, 2, { 1 }, { 0 } },
	{ 18, 0, 0, "Black", ADA::eAMScreen, 0, 45, 50, ADA::eAdobeRound, 0.6f, ADA::eProcess, 2, { 1 }, { 0 } }
};

static const ADA::TMediaSize	kMedia[] =
{
	{ "44 inch roll",	false,	true,	44 inch,	50000 mm,	0, 0, 0, 0 },
	{ "36 inch roll",	false,	true,	36 inch,	50000 mm,	0, 0, 0, 0 },
	{ "24 inch roll",	false,	true,	24 inch,	50000 mm,	0, 0, 0, 0 },
	{ "A0",			false,	false,	841 mm,		1189 mm,	0, 0, 0, 0 },
	{ "A1",			false,	false,	594 mm,		841 mm,		0, 0, 0, 0 },
	{ "A2",			false,	false,	420 mm,		594 mm,		0, 0, 0, 0 },
	{ "A3+",		false,	false,	329 mm,		483 mm,		0, 0, 0, 0 },
	{ "A3",			false,	false,	297 mm,		420 mm,		0, 0, 0, 0 },
	{ "A4",			false,	false,	210 mm,		297 mm,		0, 0, 0, 0 },
	{ "B0",			false,	false,	1030 mm,	1456 mm,	0, 0, 0, 0 },
	{ "B1",			false,	false,	728 mm,		1030 mm,	0, 0, 0, 0 },
	{ "B2",			false,	false,	515 mm,		728 mm,		0, 0, 0, 0 },
	{ "B3",			false,	false,	364 mm,		515 mm,		0, 0, 0, 0 },
	{ "B4",			false,	false,	257 mm,		364 mm,		0, 0, 0, 0 },
	{ "B5",			false,	false,	182 mm,		257 mm,		0, 0, 0, 0 },
	{ "US B",		false,	false,	11 inch,	17 inch,	0, 0, 0, 0 },
	{ "US C",		false,	false,	17 inch,	22 inch,	0, 0, 0, 0 },
	{ "US D",		false,	false,	22 inch,	34 inch,	0, 0, 0, 0 },
	{ "US E",		false,	false,	34 inch,	44 inch,	0, 0, 0, 0 },
	{ "Legal",		false,	false,	8.5f inch,	14 inch,	0, 0, 0, 0 }
};

static const ADA::TTray	kTrays[] =
{
	{ "Roll", true, 0, 0 },
	{ "Cassette", false, 0, 0 }
};

static const char* kScript_Contone =
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n\
<DriverSettings>\n\
	<POPUP ID=\"DPI\" label=\"DPI:\" selected=\"DPI_5\" changable=\"true\">\n\
		<ITEM ID=\"DPI_1\" label=\"50x50\">DPI=50,50</ITEM>\n\
		<ITEM ID=\"DPI_2\" label=\"100x100\">100,100</ITEM>\n\
		<ITEM ID=\"DPI_3\" label=\"150x150\">150,150</ITEM>\n\
		<ITEM ID=\"DPI_4\" label=\"200x200\">200,200</ITEM>\n\
		<ITEM ID=\"DPI_5\" label=\"300x300\">300,300</ITEM>\n\
		<ITEM ID=\"DPI_6\" label=\"400x400\">400,400</ITEM>\n\
		<ITEM ID=\"DPI_7\" label=\"600x600\">600,600</ITEM>\n\
		<ITEM ID=\"DPI_8\" label=\"600x300\">600,300</ITEM>\n\
		<ITEM ID=\"DPI_9\" label=\"720x720\">720,720</ITEM>\n\
	</POPUP>\n\
	<POPUP ID=\"Compression\" label=\"Compression:\" selected=\"None\" changable=\"true\">\n\
		<ITEM ID=\"LZW\" label=\"LZW\">1</ITEM>\n\
		<ITEM ID=\"None\" label=\"Uncompressed\">2</ITEM>\n\
	</POPUP>\n\
";

static const char* kScript_Halftone =
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n\
<DriverSettings>\n\
	<EDIT ID=\"X_resolution\" label=\"X resolution:\" minvalue=\"50\" maxvalue=\"3000\" changable=\"true\">600</EDIT>\n\
	<EDIT ID=\"Y_resolution\" label=\"Y resolution:\" minvalue=\"50\" maxvalue=\"3000\" changable=\"true\">600</EDIT>\n\
	<POPUP ID=\"Compression\" label=\"Compression:\" selected=\"CCITT\" changable=\"true\">\n\
		<ITEM ID=\"CCITT\" label=\"CCITT group 4\">0</ITEM>\n\
		<ITEM ID=\"LZW\" label=\"LZW\">1</ITEM>\n\
		<ITEM ID=\"None\" label=\"Uncompressed\">2</ITEM>\n\
	</POPUP>\n\
";

static const char* kScript_PlatformIntel =
"\
	<POPUP ID=\"Byte_order\" label=\"Byte order:\" selected=\"Intel\" changable=\"true\">\n\
		<ITEM ID=\"Intel\" label=\"Intel\">Intel</ITEM>\n\
		<ITEM ID=\"PPC\" label=\"Macintosh\">PPC</ITEM>\n\
	</POPUP>\n\
";

static const char* kXMLEnd = "</DriverSettings>\r";

DLLAPI	const ADA::ModelID*	CALLBACK	ADA::ListModels()
{
	return kDriverID;
}

static	ExceptionCode	Info( ADA::Instance* driverObj, ADA::DriverInfo* info, const char* xmlSettings )
{
	memset( info, 0, sizeof(ADA::DriverInfo) );

	int	m = 0;
	for( int i = 0; ADA::kDriverID[i].name != NULL; ++i )
		if( !strcmp( ADA::kDriverID[i].name, driverObj->mModelName ) )
		{
			m = i;
			break;
		}

	strcpy( info->profileName, ADA::kDriverID[m].profileName );
	info->uniqueDriverID = ADA::kDriverID[m].uniqueDriverID;
	info->maxWidth = ADA::kDriverID[m].maxWidth;
	info->maxHeight = ADA::kDriverID[m].maxHeight;
	info->caps.print = true;
	info->conns.folder = true;
	//
	//	Media size list
	//
	info->mediaCount = sizeof(kMedia) / sizeof(ADA::TMediaSize);
	info->media = new ADA::TMediaSize[ info->mediaCount ];
	memcpy( info->media, kMedia, sizeof(kMedia) );
	//
	//	Tray list
	//
	info->trayCount = sizeof(kTrays) / sizeof(ADA::TTray);
	info->tray = new ADA::TTray[ info->trayCount ];
	memcpy( info->tray, kTrays, sizeof(kTrays) );
	for( int32_t t = 0; t != info->trayCount; ++t )
	{
		info->tray[t].width = info->maxWidth;
		info->tray[t].length = info->maxHeight;
	}
	//
	//	Create XML UI script
	//
	size_t	len = strlen( m == TIFF_CT ? kScript_Contone : kScript_Halftone ); 
	len += strlen( kScript_PlatformIntel );
	len += strlen( kXMLEnd );
	info->script = new char[ len + 1 ];
	strcpy( info->script, m == TIFF_CT ? kScript_Contone : kScript_Halftone );
	strcat( info->script, kScript_PlatformIntel );
	strcat( info->script, kXMLEnd );
	//
	//	Set resolutions
	//
	info->dpuOutputX = 300;
	info->dpuOutputY = 300;
	const char* tag;
	if( m == TIFF_CT )
	{
		if( ( tag = strstr( xmlSettings, "<DPI>" ) ) != NULL )
			sscanf( tag, "<DPI>%f,%f", &info->dpuOutputX, &info->dpuOutputY );
	}
	else
	{
		if( ( tag = strstr( xmlSettings, "<X_resolution>" ) ) != NULL )
			sscanf( tag, "<X_resolution>%f", &info->dpuOutputX );
		if( ( tag = strstr( xmlSettings, "<Y_resolution>" ) ) != NULL )
			sscanf( tag, "<Y_resolution>%f", &info->dpuOutputY );
	}
	info->dpuOutputX /= 18.0f;
	info->dpuOutputY /= 18.0f;
	if( info->dpuOutputY < info->dpuOutputX )
		info->dpuRender = info->dpuOutputY;
	else
		info->dpuRender = info->dpuOutputX;
	//	In halftone mode do not render higher then 600 DPI, this is mostlikely enough CT resolution up to 2540 DPI HT
	if( m == TIFF_HT )
		if( info->dpuRender > 1200 / 18.0f )
			info->dpuRender /= 2;
		else if( info->dpuRender > 600 / 18.0f )
			info->dpuRender = 600 / 18.0f;
	//
	//	Setup ink channels
	//
	if( m == TIFF_CT )
	{
		info->inkCount = sizeof(kInks) / sizeof(ADA::TInk);
		info->inks = new ADA::TInk[ info->inkCount ];
		memcpy( info->inks, kInks, sizeof(kInks) );
		for( int32_t i = 0; i != info->inkCount; ++i )
		{
			info->inks[i].screening = ADA::eContone8;
			info->inks[i].dotCount = 256;
		}
	}
	else
	{
		info->inkCount = 1;
		info->inks = new ADA::TInk[ 1 ];
		memcpy( info->inks, kInks + 3, sizeof(ADA::TInk) );
		info->inks[0].dotCount = 2;
	}

	return NoError;
}

static void		DisposeInfo( ADA::DriverInfo* info )
{
	delete[] info->inks;
	info->inks = NULL;
	delete[] info->media;
	info->media = NULL;
	delete[] info->tray;
	info->tray = NULL;
	delete[] info->script;
	info->script = NULL;
}

typedef	const char * const	ConstStr;

ConstStr	kPrinterStatus				= "PrinterStatus",
			kPrinterStatusInks			= "Inks",
			kPrinterStatusInk			= "Ink",
			kPrinterStatusInkCapacity	= "Capacity",
			kPrinterStatusInkLevel		= "Level",
			kPrinterStatusInkStatus		= "Status",
			kPrinterStatusTemperatures	= "Temperatures",
			kPrinterStatusTemperature	= "Temperature",
			kPrinterStatusTempName		= "Name",
			kPrinterStatusTempTarget	= "Target",
			kPrinterStatusTempMax		= "Max",
			kPrinterStatusTrays			= "Trays",
			kPrinterStatusTray			= "Tray",
			kPrinterStatusTrayType		= "Type",
			kPrinterStatusMediaName		= "Media",
			kPrinterStatusMediaWidth	= "Width",
			kPrinterStatusMediaHeight	= "Height",
			kPrinterStatusMediaMargins	= "Margins",
			kPrinterStatusMessage		= "Message",
			kPrinterStatusList			= "ErrorList",
			kPrinterStatusSeverity		= "Severity";

static void PrinterStatus( ADA::Instance* driverObj, ACPL::XML* statusDict )
{
	//
	//	Spoof ink level reporting
	//
	statusDict->Initialize( kPrinterStatus );
	ACPL::XML::Node*	inks = statusDict->root->AddChildNode( kPrinterStatusInks );
	for( size_t i = 0; i != sizeof(kInks)/sizeof(ADA::TInk); ++i )
	{
		ACPL::XML::Node*	ink = inks->AddChildNode( kPrinterStatusInk );
		ink->SetValue( kInks[i].name );
		ink->AddAttribute( kPrinterStatusInkCapacity, 1000 );	//	Simulate 1000 ml bottles
		ink->AddAttribute( kPrinterStatusInkLevel, ::rand() * 1000 / RAND_MAX );
	}
}

static const ADA::InstanceMethods	kInstanceMethods =
{
	Info,
	DisposeInfo,
	CreateSession,
	DisposeSession,
	PrinterStatus,
	nullptr,
	nullptr,
	nullptr,
	nullptr
};

DLLAPI	aur::ADA::Instance*	CALLBACK	aur::ADA::PrinterInitialize( const char* modelName, const char* url, const InstanceMethods** methods )
{
	*methods = &kInstanceMethods;
	return new ADA::Instance( modelName, url );
}

DLLAPI	bool	CALLBACK	aur::ADA::PrinterUpdateDriver( ADA::Instance* driverObj, const char* proxy, uint8_t region, void* hostObj, ADA::DriverInitCB progressCB )
{
	return NoError;
}

DLLAPI	void	CALLBACK	aur::ADA::PrinterTerminate( ADA::Instance* driverObj )
{
	delete driverObj;
}
