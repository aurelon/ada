#pragma once

#include <ADA/DriverInfo.h>
#include <ACPL/CString.h>
#include <ACPL/FileSpec.h>

using namespace aur;

typedef	struct tiff TIFF;

#define	TIFF_CT	0
#define	TIFF_HT 1

class	ADA::Instance
{
public:
	ACPL::String	mModelName;
	ACPL::String	mDeviceURL;
	
	inline	Instance( const char* model, const char* url ) : mModelName( model ), mDeviceURL( url ) {}
};

class CTIFFDriver
{
	int					mModel;
	ACPL::String		mURI;
	ACPL::FileSpec		mFolderPath;
	int32_t				mLineSize;
	int32_t				mLine;
	uint8_t				mLastPlane;
	uint8_t*			mLineBuffer;
	ACPL::UString		mFileName;
	float				mDPI_X;
	float				mDPI_Y;
	int					mCompression;
	TIFF*				mLibTif;
public:
						CTIFFDriver( const char* modelName, const char* deviceURI );
						~CTIFFDriver();
	ExceptionCode		Initialize( float dpuX, float dpuY, const char* xmlSettings );
			void		JobStart( const ACPL::UString& jobName );
			void		Abort();
			void		RasterStart( int32_t width, int32_t height );
			void		RasterEnd();
			void		ProcessRawBuffer( const uint8_t* planeData, int32_t dataBytes, uint8_t planeIdx );
};

void*	CreateSession( ADA::Instance* driverObj, const ADA::DriverMethods** method, const char* deviceURL );
void	DisposeSession( ADA::Instance* driverObj,void* cookie );
