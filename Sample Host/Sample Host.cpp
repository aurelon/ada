// Sample Host.cpp : Defines the entry point for the console application.
//

#include <windows.h>
#include <ADA/DriverInfo.h>
#include <ACPL/CString.h>

using namespace aur;

int main(int argc, char* argv[])
{
	typedef ADA::ModelID* (CALLBACK *ListModels)();
	typedef ADA::Instance* (CALLBACK *PrintInit)( const char*, const char*, ADA::InstanceMethods** );
	typedef void (CALLBACK *PrintTerm)( ADA::Instance* );

	void* symAddr;
	//
	//	Load the driver
	//
	wchar_t	path[1024];
	::GetModuleFileNameW( ::GetModuleHandle( NULL ), path, sizeof(path) );
	::wcsrchr( path, '\\' )[1] = 0;
	::wcscat( path, L"Sample Driver.amd" );

	HMODULE	dll = ::LoadLibraryW( path );
	if( dll == NULL )
		exit(1);
	//
	//	Get information from the driver
	//
	//	List all models supported by the driver
	//
	symAddr = ::GetProcAddress( dll, "?ListModels@ADA@aur@@YAPEBUModelID@12@XZ" );
	if( symAddr == NULL )
		exit(2);
	ListModels	listModels = ListModels( symAddr );
	ADA::ModelID*	list = listModels();
	for( uint32_t i = 0; list[i].name; ++i )
	{
		printf( "Driver #%u : %s\n", i, list[i].name );
	}
	//
	//	Create driver instance, keep it alive during the full use of the program
	//	do not create/dispose it for each print
	//
	symAddr = ::GetProcAddress( dll, "?PrinterInitialize@ADA@aur@@YAPEAVInstance@12@PEBD0PEAPEBUInstanceMethods@12@@Z" );
	if( symAddr == NULL )
		exit(3);
	PrintInit printInit = PrintInit( symAddr );
	ADA::InstanceMethods*	instanceMethods;
	ADA::Instance* driverObj = printInit( list[0].name, "file://C:\\Users\\Public\\", &instanceMethods );
	if( driverObj == NULL )
		exit(3);
	//
	//	Get default settings and UI script from the first
	//	driver.
	//
	//	This call is usually done by the RIP UI to show the
	//	settings of the driver.
	//
	ADA::DriverInfo*	info = new ADA::DriverInfo();
	instanceMethods->Info( driverObj, info, "" );
	printf( "%s\n", info->script );
	for( int32_t i = 0; i != info->inkCount; ++i )
		printf( "%s (%u levels)\n", info->inks[i].name, info->inks[i].dotCount );
	delete info;
	//
	//	Get settings based on UI settings
	//
	//	This call is usually done to get the RIP settings
	//	like resolution and channels. The results from the UI
	//	are for demo purposes hardcoded in the source (kScriptResults).
	//
	const char*	kScriptResults =
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n\
<DS>\n\
	<DPI>300,300</DPI>\n\
	<Compression>1</Compression>\n\
	<Byte_order>Intel</Byte_order>\n\
</DS>\n\
";
	info = new ADA::DriverInfo();
	instanceMethods->Info( driverObj, info, kScriptResults );
	printf( "DPU=%.2f x %.2f\n", info->dpuOutputX, info->dpuOutputY );
	//
	//	After or during RIP the actual driver processor is called
	//
	//	1. Create a driver reference
	//
	const ADA::DriverMethods*		methods;
	void* objRef = instanceMethods->CreateSession( driverObj, &methods, NULL );
	if( objRef == NULL )
		exit(5);
	bool	isInited = false;
	int		imageWidth = 256;
	int		imageHeight = 512;
	try
	{
		//
		//	Open connection / Initialize
		//
		if( methods->Initialize )
		{
			ExceptionCode err = methods->Initialize( objRef, NULL, info->dpuOutputX, info->dpuOutputY, kScriptResults );
			if( err )
				throw err;
		}
		isInited = true;	
		//
		//	Open Job
		//
		if( methods->JobStart )
		{
			int32_t	copies = 1;
			methods->JobStart( objRef, L"Test job", copies, info->media[0] /* Legal */, info->tray[1] /* Cassette */, true, false );
		}
		//
		//	Start page
		//
		ADA::PageRect	pageRect;
		pageRect.left = 0;
		pageRect.top = 0;
		pageRect.right = imageWidth / info->dpuOutputX;
		pageRect.bottom = imageHeight / info->dpuOutputY;
		if( methods->PageStart )
			methods->PageStart( objRef, 1 /* Page 1 */, 'S' /* Single sided */, "" /* Barcode */, info->media[0] /* Legal */, info->tray[1] /* Cassette */, pageRect, L"" );
		//
		//	Start image
		//
		if( methods->RasterStart )
			methods->RasterStart( objRef, 0, 0, imageWidth, imageHeight );
		//
		//	Image data
		//
		//	Provide line by line, ink by ink
		//	Trucate data to remove trailing 0's
		//
		uint8_t*	sampleDataLine = new uint8_t[imageWidth];
		for( int i = 0; i != imageWidth; ++i )
			sampleDataLine[i] = i;
		for( int line = 0; line != imageHeight; ++line )
			for( uint8_t ink = 0; ink != info->inkCount; ++ink )
			{
				const uint8_t* data = NULL;
				int32_t	dataLen = 0;
				//	Image synthezing
				if( ink == line / ( imageHeight / 4 ) )
				{
					data = sampleDataLine;
					dataLen = 256 - ( line % 256 );
				}
				//	Actual driver call
				methods->ProcessRawBuffer( objRef, data, dataLen, ink );
			}
		delete[] sampleDataLine;
		//
		//	End page
		//
		if( methods->RasterEnd )
			methods->RasterEnd( objRef );
		//
		//	End page
		//
		if( methods->PageEnd )
			methods->PageEnd( objRef, 1 );
		//
		//	Close Job
		//
		if( methods->JobEnd )
			methods->JobEnd( objRef );
		//
		//	Close connection / Terminate
		//
		if( methods->Terminate )
			methods->Terminate( objRef, false );
	}
	catch( ExceptionCode driverErr )
	{
		if( isInited )
			if( methods->Terminate )
				methods->Terminate( objRef, true /* abort */ );
		driverErr;	/* report error */
	}
	//
	//	x. Dispose the driver reference
	//
	instanceMethods->DisposeSession( driverObj, objRef );
	delete info;
	//
	//	Terminate printer instance at the end of the application
	//
	symAddr = ::GetProcAddress( dll, "?PrinterTerminate@ADA@aur@@YAXPEAVInstance@12@@Z" );
	if( symAddr == NULL )
		exit(6);
	PrintTerm printTerm = PrintTerm( symAddr );
	printTerm( driverObj );
	//
	//	Unload the driver
	//
	::FreeLibrary( dll );
	return 0;
}
