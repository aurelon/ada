#pragma once

#include <memory>
#include "./ACPLSignal.h"
#include "./XML.h"
#include "./MemoryStream.h"

namespace aur { namespace ACPL {

	class ACPLAPI IPC
	{
	public:
		enum ConnectionStatus
		{
			eConnected = 0,
			eNotConnected,
			eDisconnected
		};

		class ACPLAPI ConnectData
		{
		public:
									ConnectData( const String& connectionString, const int32_t connectionport, const bool isServer, const bool keepServerRunning = false );
									ConnectData( const ConnectData& other );
			virtual					~ConnectData();
					const String&	GetConnectionString() const;
					int32_t			GetConnectionPort() const;
					bool			IsServer() const;
					bool			GetKeepServerRunning() const;

					ConnectData&	operator=( const ConnectData &other );
		protected:
			String					mConnectionString;
			int32_t					mConnectionPort;
			bool					mIsServer;
			bool					mKeepServerRunning;
		};

		class ACPLAPI Message		// message base class 
		{
		protected:
					uint32_t		mMessageId;
		public:
									Message( uint32_t );
			virtual					~Message();
					void			SetMessageID( uint32_t );
					uint32_t		GetMessageID();

			virtual Message&		operator>>(Stream&) = 0;
			virtual Message&		operator<<(Stream&) = 0;

			static const uint32_t	KeepAliveMessageType;
		};

									IPC();
		virtual						~IPC();

		virtual bool				Connect( const ConnectData* ) = 0;
		virtual bool				Send( Message* ) = 0;
		virtual bool				Disconnect() = 0;

		ConnectionStatus			GetConnectionStatus() const;
		Signal<Stream*>*			ReceiveSignal();
		Signal<IPC*>*				DisconnectedSignal();

	protected:
		ConnectionStatus			mConnectionStatus;

		Signal<Stream*>*			mReceiveSignal;
		Signal<IPC*>*				mDisconnectedSignal;
	};

	inline const String& IPC::ConnectData::GetConnectionString() const
	{
		return mConnectionString;
	}

	inline int32_t IPC::ConnectData::GetConnectionPort() const
	{
		return mConnectionPort;
	}

	inline bool IPC::ConnectData::IsServer() const
	{
		return mIsServer;
	}

	inline bool IPC::ConnectData::GetKeepServerRunning() const
	{
		return mKeepServerRunning;
	}

	inline IPC::ConnectData& IPC::ConnectData::operator=( const IPC::ConnectData &other )
	{
		if( this == &other )
			return *this;

		mConnectionString = other.mConnectionString;
		mConnectionPort = other.mConnectionPort;
		mIsServer = other.mIsServer;
		mKeepServerRunning = other.mKeepServerRunning;

		return *this;
	}
	
	inline Signal<Stream*>* IPC::ReceiveSignal() 
	{
		return mReceiveSignal;
	}

	inline Signal<IPC*>* IPC::DisconnectedSignal()
	{
		return mDisconnectedSignal;
	}

	inline IPC::ConnectionStatus IPC::GetConnectionStatus() const
	{
		return mConnectionStatus;
	}

	inline IPC::Message::Message( uint32_t id ) : mMessageId( id )
	{
	}

	inline void IPC::Message::SetMessageID( uint32_t id )
	{
		mMessageId = id;
	}

	inline uint32_t IPC::Message::GetMessageID()
	{
		return mMessageId;
	}

	//
	// customized IPC messages
	//
	class ACPLAPI IPCMessageXML : public IPC::Message, public XML
	{
	public:
								IPCMessageXML( uint32_t );
		virtual					~IPCMessageXML();
		virtual IPC::Message&	operator>>(Stream&);
		virtual IPC::Message&	operator<<(Stream&);
	};

	class ACPLAPI IPCMessageMemoryStream : public IPC::Message
	{
		MemoryStream			mMemStream;
	public:
								IPCMessageMemoryStream( uint32_t );
		virtual					~IPCMessageMemoryStream();
		virtual IPC::Message&	operator>>(Stream&);
		virtual IPC::Message&	operator<<(Stream&);
								operator MemoryStream&();
	};

}}
