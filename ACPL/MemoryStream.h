// ===========================================================================
//	MemoryStream.h			  (c) 1998-2012 Aurelon. All rights reserved.
// ===========================================================================
//
//	A Stream whose bytes are in a block in memory

#pragma once

#include "./Stream.h"

namespace aur { namespace ACPL {

	class ACPLAPI MemoryStream : public Stream
	{
		char*					mDataH;
		int32_t					mDataSize;
	public:
								MemoryStream();
								MemoryStream( char*, int32_t );
								MemoryStream( const MemoryStream& );
		virtual					~MemoryStream();
				MemoryStream&	operator=( const MemoryStream& );
		virtual void			SetLength( int64_t );

		virtual ExceptionCode	PutBytes( const void*, int32_t& );
		virtual ExceptionCode	GetBytes( void*, int32_t& );

				void			SetDataPtr( char*, int32_t );
				char*			GetDataPtr();
				void			DetachDataPtr();
	};

	inline char* MemoryStream::GetDataPtr()
	{
		return mDataH;
	}

	inline void MemoryStream::DetachDataPtr()
	{
		mDataH = NULL;
		mDataSize = 0;
		SetDataPtr( NULL, 0 );
	}

}}
