#pragma once

#include "./Types.h"

void			ACPLAPI ConcatString( aur::StringPtr str1, aur::ConstStringPtr str2 );
void			ACPLAPI ConcatString( aur::StringPtr str1, const char *str2 );
aur::StringPtr	ACPLAPI CopyString( aur::StringPtr str1, aur::ConstStringPtr str2 );
aur::StringPtr	ACPLAPI CopyString( aur::StringPtr str1, const char *str2 );
aur::StringPtr	ACPLAPI CopyString( aur::StringPtr str1, const char *str2, int16_t len );
int16_t			ACPLAPI StringCompare( aur::ConstStringPtr str1, aur::ConstStringPtr str2 );
bool			ACPLAPI StringICompare( aur::ConstStringPtr str1, aur::ConstStringPtr str2 );
bool			ACPLAPI EqualBytes( const void *s1, const void *s2, uint32_t n );
int32_t			ACPLAPI PackRLE( const uint8_t* src, uint8_t* dest, int32_t size );
int32_t			ACPLAPI UnpackRLE( const uint8_t* src, uint8_t* dest, int32_t size );

#define			ClearStruct( x )		::memset( &(x), 0, sizeof(x) )
#define			ClearBytes( x, y )		::memset( x, 0, y )
#define			MoveBytes( x, y, z )	::memmove( y, x, z )
