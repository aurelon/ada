#pragma once

#include "IPC.h"

#if ACPL_MAC || ACPL_LINUX
typedef	int SOCKET;
#	define SOCKET_ERROR -1
#	define INVALID_SOCKET -1
#elif ACPL_WIN
#	include <WS2tcpip.h>
#endif

namespace aur { namespace ACPL {

	class TcpIPCServer;

	class ACPLAPI TcpIPC : public IPC, public Thread
	{
		friend TcpIPCServer;
	public:
		
								TcpIPC();
		virtual					~TcpIPC();

		virtual bool			Connect( const IPC::ConnectData* connection );
		virtual bool			Send( IPC::Message* message );
		virtual bool			Disconnect();
	
	protected:
		virtual void			ThreadMain();

		bool					SendMessage( SOCKET socket, IPC::Message* message );
		MemoryStream*			ReceiveMessage();		
		void					Receive();

	private:
		bool					ConnectSocket( SOCKET& socket );
		
	private:
		TcpIPCServer*			mServer;
		IPC::ConnectData		mConnectionData;
		SOCKET					mSendSocket;
		SOCKET					mReceiveSocket;
		uint16_t				mServerPort;
		volatile bool			mConnected;
	};

	class ACPLAPI TcpIPCServer : public Thread
	{
	public:
		TcpIPCServer( TcpIPC* tcpIpc );
		virtual					~TcpIPCServer();

	protected:
		virtual void			ThreadMain();

	private:
		TcpIPC*					mIpc;
		SOCKET					mServerSocket;
		bool					mReceiveConnected;
	};
}}
