#pragma once

#include "./ACPLSignalSlotBase.h"

namespace aur { namespace ACPL {

/*!	\brief	Class representing a signal

Template class representing a definition of a Signal with maximum 10 method arguments.
This class can be used to connect to both an Instance or Static Slot.

\sa Slot, SignalSlotBase
*/
template <
			class Arg01Type = Undefined,
			class Arg02Type = Undefined,
			class Arg03Type = Undefined,
			class Arg04Type = Undefined,
			class Arg05Type = Undefined,
			class Arg06Type = Undefined,
			class Arg07Type = Undefined,
			class Arg08Type = Undefined,
			class Arg09Type = Undefined,
			class Arg10Type = Undefined
		>
class Signal : public SignalSlotBaseTemplate<Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type>
{
	typedef SignalSlotBaseTemplate
			<
				Arg01Type,
				Arg02Type,
				Arg03Type,
				Arg04Type,
				Arg05Type,
				Arg06Type,
				Arg07Type,
				Arg08Type,
				Arg09Type,
				Arg10Type
			>
		SignalSlotBaseTemplateType;

	typedef Array<SignalSlotBaseStandard*> SignalSlotList;

public:
	// Constructor
			/*!	\brief	Default constructor
			*/
			Signal()
				: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSignal)
			{
			}

			/*!	\brief	Constructor from slot

			Constructor that builds a signal and connects it to a slot.

			\param[in]	slot			The slot to connect to
			\param[in]	connectionType	Specifies whether synchronous, asynchronous or blocking asynchronous connection should be made
			\return		void
			*/
			Signal(SignalSlotBaseStandard* slot, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
				: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSignal)
			{
				Connect(slot, connectionType);
			}

			Signal(SignalSlotBaseStandard& slot, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
				: SignalSlotBaseTemplateType(SignalSlotBaseStandard::eSignal)
			{
				Connect(&slot, connectionType);
			}

	// Methods
			/*!	\brief	Connect to a slot

			This method connects to a slot.

			\param[in]	slot	The slot to connect to
			\return		void
			*/
			void		Connect(SignalSlotBaseStandard* slot, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
			{
				SignalSlotBaseTemplateType::ConnectEntity(slot, connectionType);
			}

			void		Connect(SignalSlotBaseStandard& slot, SignalSlotBaseStandard::ConnectionKind connectionType = SignalSlotBaseStandard::eSynchronous)
			{
				SignalSlotBaseTemplateType::ConnectEntity(&slot, connectionType);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 0 parameters.
			This method will trigger the execution of each linked slot's method.

			\return		void
			*/
			void		Emit()
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit();

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync();

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync();
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 1 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 2 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 3 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 4 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\param[in]	arg04	Fourth parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03, arg04);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03, arg04);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03, arg04);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 5 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\param[in]	arg04	Fourth parameter
			\param[in]	arg05	Fifth parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03, arg04, arg05);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03, arg04, arg05);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03, arg04, arg05);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 6 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\param[in]	arg04	Fourth parameter
			\param[in]	arg05	Fifth parameter
			\param[in]	arg06	Sixth parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03, arg04, arg05, arg06);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03, arg04, arg05, arg06);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 7 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\param[in]	arg04	Fourth parameter
			\param[in]	arg05	Fifth parameter
			\param[in]	arg06	Sixth parameter
			\param[in]	arg07	Seventh parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03, arg04, arg05, arg06, arg07);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 8 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\param[in]	arg04	Fourth parameter
			\param[in]	arg05	Fifth parameter
			\param[in]	arg06	Sixth parameter
			\param[in]	arg07	Seventh parameter
			\param[in]	arg08	Eighth parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 9 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\param[in]	arg04	Fourth parameter
			\param[in]	arg05	Fifth parameter
			\param[in]	arg06	Sixth parameter
			\param[in]	arg07	Seventh parameter
			\param[in]	arg08	Eighth parameter
			\param[in]	arg09	Ninth parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09);
			}

			/*!	\brief	Emits the execution of all underlying slots

			Method that emits the execution of the event.
			This method should be called in case a pair of signal/slot objects was created and linked to a method with 10 parameters.
			This method will trigger the execution of each linked slot's method.

			\param[in]	arg01	First parameter
			\param[in]	arg02	Second parameter
			\param[in]	arg03	Third parameter
			\param[in]	arg04	Fourth parameter
			\param[in]	arg05	Fifth parameter
			\param[in]	arg06	Sixth parameter
			\param[in]	arg07	Seventh parameter
			\param[in]	arg08	Eighth parameter
			\param[in]	arg09	Ninth parameter
			\param[in]	arg10	Tenth parameter
			\return		void
			*/
			void		Emit(Arg01Type arg01, Arg02Type arg02, Arg03Type arg03, Arg04Type arg04, Arg05Type arg05, Arg06Type arg06, Arg07Type arg07, Arg08Type arg08, Arg09Type arg09, Arg10Type arg10)
			{
				for( uint32_t i = 0; i != this->mCounterSyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterSyncEntities[i])->Emit(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);

				for( uint32_t i = 0; i != this->mCounterAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterAsyncEntities[i])->EmitAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);

				for( uint32_t i = 0; i != this->mCounterBlockingAsyncEntities.GetCount(); ++i )
					static_cast<SignalSlotBaseTemplateType*>(this->mCounterBlockingAsyncEntities[i])->EmitBlockingAsync(arg01, arg02, arg03, arg04, arg05, arg06, arg07, arg08, arg09, arg10);
			}

protected:
			int32_t		FinalizeEmitAsync(void*, int32_t) { return false; }
			void		EmitAsync() {}
			void		EmitAsync(Arg01Type) {}
			void		EmitAsync(Arg01Type, Arg02Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type) {}
			void		EmitAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type) {}
			void		EmitBlockingAsync() {}
			void		EmitBlockingAsync(Arg01Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type) {}
			void		EmitBlockingAsync(Arg01Type, Arg02Type, Arg03Type, Arg04Type, Arg05Type, Arg06Type, Arg07Type, Arg08Type, Arg09Type, Arg10Type) {}
};

}}
