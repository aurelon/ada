#pragma once
#include "CString.h"

//UUID  = time-low "-" time-mid "-" time-high-and-version "-" clock-seq-and-reserved clock-seq-low "-" node

namespace aur { namespace ACPL {

	class ACPLAPI Guid
	{
		uint8_t	uuid[16];
	public:
		static Guid		Generate();

		Guid( bool gen = false );
		Guid( const char* );
		String	ToString() const;
		inline operator String() const { return ToString(); };
		bool IsEmpty() const;
		bool operator==( const Guid& ) const;
		bool operator==( const char* ) const;
		bool operator!=( const Guid& ) const;
		bool operator!=( const char* ) const;
		bool operator<( const Guid& ) const;
		bool operator>( const Guid& ) const;
	};

	typedef std::vector<Guid> GuidList;

} }
