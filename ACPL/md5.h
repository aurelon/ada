#pragma once

#include "Types.h"

namespace aur { namespace ACPL {

	class	Stream;
	class	MemoryStream;
	class	String;

	/*
	 **********************************************************************
	 ** MD5.h                                                            **
	 **                                                                  **
	 ** - Style modified by Tony Ray, January 2001                       **
	 **   Added support for randomizing initialization constants         **
	 ** - Style modified by Dominik Reichl, September 2002               **
	 **   Optimized code                                                 **
	 **                                                                  **
	 **********************************************************************
	 */

	/*
	 **********************************************************************
	 ** MD5.h -- Header file for implementation of MD5                   **
	 ** RSA Data Security, Inc. MD5 Message Digest Algorithm             **
	 ** Created: 2/17/90 RLR                                             **
	 ** Revised: 12/27/90 SRD,AJ,BSK,JT Reference C version              **
	 ** Revised (for MD5): RLR 4/27/91                                   **
	 **   -- G modified to have y&~z instead of y&z                      **
	 **   -- FF, GG, HH modified to add in last register done            **
	 **   -- Access pattern: round 2 works mod 5, round 3 works mod 3    **
	 **   -- distinct additive constant for each step                    **
	 **   -- round 4 added, working mod 7                                **
	 **********************************************************************
	 */

	/* Data structure for MD5 (Message Digest) computation */
	class	ACPLAPI	MD5
	{
		uint32_t	i[2];		/* Number of _bits_ handled mod 2^64 */
		uint32_t	buf[4];		/* Scratch buffer */
		uint8_t		in[64];		/* Input buffer */
	public:
					MD5( uint32_t pseudoRandomNumber = 0 );
		void		Update( const void* inBuf, size_t inLen );
		void		Update( Stream& );
		void		Update( MemoryStream& );
		void		Final( uint8_t digest[16] );
		String		Hash( const String& inputStr );
		String		Hash( Stream& memStream );
	};


	/*
	 sha1.cpp - source code of

	 ============
	 SHA-1 in C++
	 ============

	 100% Public Domain.

	 Original C Code
	 -- Steve Reid <steve@edmweb.com>
	 Small changes to fit into bglibs
	 -- Bruce Guenter <bruce@untroubled.org>
	 Translation to simpler C++ Code
	 -- Volker Grabsch <vog@notjusthosting.com>
	 */
	class ACPLAPI SHA1
	{
	public:
		SHA1();
		void	Update( const String& s );
		void	Update( Stream& is );
		String	Final();
		String	Hash( const String& inputStr );
		String	Hash( Stream& memStream );

		static String sha1( const String& );

	private:
		static const unsigned int DIGEST_INTS = 5;  /* number of 32bit integers per SHA1 digest */
		static const unsigned int BLOCK_INTS = 16;  /* number of 32bit integers per SHA1 block */
		static const unsigned int BLOCK_BYTES = BLOCK_INTS * 4;

		uint32_t digest[DIGEST_INTS];
		uint8_t mInput[BLOCK_BYTES];
		int32_t mInputFill;
		uint64_t transforms;

		void reset();
		void transform( uint32_t block[BLOCK_BYTES]);

		void input_to_block( uint32_t block[BLOCK_BYTES]);
	};
}}
