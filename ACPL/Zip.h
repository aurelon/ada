// Zip.h: interface for the Zip class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "./CString.h"
#include "./Stream.h"
#include "./Array.h"
#include "./FileSpec.h"

namespace aur { namespace ACPL {

	class ACPLAPI Zip
	{
		class Zipper;
		class Unzipper;
	public:
		static bool ZipFolder( const UString& FilePath, const UString& ToFolder, String password = String() );
		static bool ZipFile( const UString& FilePath, const UString& ToFolder );
		static bool	ZipFiles( Array<UString>& filesToZip, const UString& ToFolder, String password = String() );
		static bool Unzip(const UString& FilePath, const UString& ToFolder, String password = String() );
		static int	GetUnzippedSize();
		static int	GetTotalFilesNr( const UString& FilePath );
		static int	GetCurrent();
		static void SetCurrent(int n);
		static void GetRootFiles( const UString& FilePath, Array<UString>& );
	};

	class ACPLAPI UnzipStream : public Stream
	{
				void*			mUzFile;
				bool			mFileOpen;
				uint8_t*		mBuffer;
				uint32_t		mBufferFill;
	public:
								UnzipStream( const FileSpec&, const char* );
		virtual					~UnzipStream();
		virtual void			SetMarker( int64_t, StreamFrom );
		virtual int64_t			GetMarker() const;
		virtual void			SetLength( int64_t );
		virtual int64_t			GetLength() const;
		virtual ExceptionCode	GetBytes( void *outBuffer, int32_t &ioByteCount );
		virtual ExceptionCode	PutBytes( const void *inBuffer, int32_t &ioByteCount );
				bool			FileFound() const;
	};

	inline	bool	UnzipStream::FileFound() const
	{
		return mUzFile != NULL;
	}

	class ACPLAPI ZipArchive
	{
	public:
					ZipArchive( const FileSpec& zip );
					~ZipArchive();
		bool		AddFile( const FileSpec& spec, const ACPL::UString* storeAs = NULL );
		bool		AddFile( Stream& stream, UString asName );
		bool		AddFolder( const FileSpec& spec );
		void		Close();
		void		SetRootFolder( const FileSpec& spec );
	private:
		void*		mUzFile;
		FileSpec	mRootFolder;
	};

	class ACPLAPI UnZipArchive
	{
	public:
		class Info
		{
		public:
			ACPL::UString	mFileName;
			ACPL::String	mComment;

			bool mFolder;
			uint32_t mVersion;
			uint32_t mVersionNeeded;
			uint32_t mFlags;
			uint32_t mCompressionMethod;
			time_t mTime;
			uint32_t mCompressedSize;
			uint32_t mUncompressedSize;
		};

					UnZipArchive( const FileSpec& zip );
					~UnZipArchive();
		int			GetFileCount();
		bool		GotoFirstFile();
		bool		GotoNextFile();
		UString		GetFileName();
		Info		GetInfo();
		bool		UnZipFile( const FileSpec& destFolder, const char* password = NULL );
		bool		UnZipFile( Stream& stream, const char* password = NULL );
	private:
		void*		mUzFile;
	};

}}
