/*
 * Array.h
 * Copyright (c) 1997-2012 Aurelon. All rights reserved.
 *
 * Template class for a generic array
 */

#pragma once

#include "./Types.h"
#include <string.h>

namespace aur { namespace ACPL {

	template<class T>
	class Array
	{
			uint32_t	mFilled;
			uint32_t	mMax;
			uint32_t	mStep;
			T*			mItem;
	public:
						Array( uint32_t = 1, uint32_t = 1 );
						~Array();
	inline	int32_t		Count() const { return mFilled; }
	inline	uint32_t	GetCount() const { return mFilled; }
			void		Append( const T& inItem );
			void		Insert( const T& inItem, uint32_t );
			void		Remove( uint32_t );
			uint32_t	Empty( bool = true );
			void		SetSize( uint32_t );
			void		SetStep( uint32_t );
			void		ShrinkToFit();

#ifdef debugging
			T&			operator[]( uint32_t ) const;
#else
	inline	T&			operator[]( uint32_t index ) const { return mItem[index]; }
#endif
	private:
			void		AdjustStorage();
	};

#ifdef debugging

	template <class T>
	T	&Array<T>::operator[]( uint32_t index ) const
	{
		if( index >= mFilled )
			LogEvent( LogLevelError, "index out of bounds" );
		return mItem[index];
	}

#endif

	template <class T>
	Array<T>::Array( uint32_t startSize, uint32_t stepSize )
	{
#ifdef debugging
		if( startSize == 0 || stepSize == 0 )
			LogEvent( LogLevelError, "Array parameter error" );
#endif
		mFilled = 0;
		mMax = startSize;
		mStep = stepSize;
		mItem = new T[ mMax ];
	}

	template <class T>
	Array<T>::~Array()
	{
		delete[] mItem;
	}

	template <class T>
	uint32_t	Array<T>::Empty( bool shrink )
	{
		size_t	freed = 0;
		if( shrink && mMax > mStep )
		{
			freed = (mMax - mStep) * sizeof(T);
			delete[] mItem;
			mMax = mStep;
			mItem = new T[ mMax ];
		}
		mFilled = 0;
		return uint32_t( freed );
	}

	template <class T>
	void	Array<T>::Append( const T& inItem )
	{
		if( mFilled == mMax )
		{
			if( ( mMax >> 3 ) > mStep )
				mMax += mMax >> 3;
			else
				mMax += mStep;
			T	park = inItem;
			AdjustStorage();
			mItem[mFilled++] = park;
		}
		else
			mItem[mFilled++] = inItem;
	}

	template <class T>
	void	Array<T>::Insert( const T& inItem, uint32_t index )
	{
#ifdef debugging
		if( index > mFilled )
			LogEvent( LogLevelError, "index out of bounds" );
#endif
		T	park = inItem;
		if( mFilled == mMax )
		{
			if( ( mMax >> 3 ) > mStep )
				mMax += mMax >> 3;
			else
				mMax += mStep;
			AdjustStorage();
		}
		if( index < mFilled )
			::memmove( &mItem[index + 1 ], &mItem[index], ( mFilled - index ) * sizeof(T) );
		else if( index > mFilled )
			index = mFilled;
		mFilled++;
		mItem[index] = park;
	}

	template <class T>
	void	Array<T>::Remove( uint32_t index )
	{
#ifdef debugging
		if( index >= mFilled )
			LogEvent( LogLevelError, "index out of bounds" );
#endif
		::memmove( &mItem[index ], &mItem[index + 1], ( mFilled - index - 1 ) * sizeof(T) );
		--mFilled;
		if( mStep > 1 && mMax - mFilled > mStep )
		{
			mMax -= mStep;
			AdjustStorage();
		}
	}

	template <class T>
	void	Array<T>::AdjustStorage()
	{
		T*	na = new T[ mMax ];
		::memcpy( na, mItem, mFilled * sizeof(T) );
		delete[] mItem;
		mItem = na;
	}

	template <class T>
	void	Array<T>::SetSize( uint32_t sz )
	{
#ifdef debugging
		if( sz < mFilled )
			LogEvent( LogLevelError, "size to small" );
#endif
		mMax = sz;
		AdjustStorage();
	}

	template <class T>
	void	Array<T>::SetStep( uint32_t step )
	{
#ifdef debugging
		if( step == 0 )
			LogEvent( LogLevelError, "step may not be zero" );
#endif
		mStep = step;
	}

	template <class T>
	void	Array<T>::ShrinkToFit()
	{
		mMax = mFilled;
		AdjustStorage();
	}

}}
