/*
 * Stream
 * Copyright (c) 1997-2012 Aurelon. All rights reserved.
 *
 * Abstract class for reading/writing an ordered sequence of bytes
 */

#pragma once

#include "./Types.h"

namespace aur { namespace ACPL {

#if ACPL_WIN
	enum StreamFrom
	{
		eFromStart = FILE_BEGIN,
		eFromEnd = FILE_END,
		eFromMarker = FILE_CURRENT
	};
#else
	enum StreamFrom
	{
		eFromStart = SEEK_SET,
		eFromEnd = SEEK_END,
		eFromMarker = SEEK_CUR
	};
#endif

	class	String;
	class	UString;

	const ExceptionCode	ErrRead		= -19;
	const ExceptionCode	ErrWrite	= -20;

	class ACPLAPI Stream
	{
	public:
								Stream();
		virtual					~Stream();

		virtual void			SetMarker( int64_t inOffset, StreamFrom inFromWhere );
		virtual int64_t			GetMarker() const;

		virtual void			SetLength( int64_t inLength );
		virtual int64_t			GetLength() const;

				bool			AtEnd() const;

							// Write Operations
		virtual ExceptionCode	PutBytes( const void*, int32_t& );
				int32_t			WriteData( const void*, int32_t );
				void			WriteBlock( const void*, int32_t );
		friend	Stream&			operator << ( Stream&, ConstStringPtr );
		friend	Stream&			operator << ( Stream&, const char* );
ACPLAPI	friend	Stream&			operator << ( Stream&, const UString& );
		friend	Stream&			operator << ( Stream&, int8_t inNum );
		friend	Stream&			operator << ( Stream&, uint8_t inNum );
		friend	Stream&			operator << ( Stream&, bool inBool );
		virtual	Stream&			operator << ( int16_t inNum );
		virtual	Stream&			operator << ( uint16_t inNum );
		virtual	Stream&			operator << ( int32_t inNum );
		virtual	Stream&			operator << ( uint32_t inNum );
		virtual	Stream&			operator << ( long inNum );
		virtual	Stream&			operator << ( unsigned long inNum );
#if ACPL_WIN || ACPL_MAC
		virtual	Stream&			operator << ( int64_t inNum );
#endif
		virtual	Stream&			operator << ( float inNum );
		virtual	Stream&			operator << ( double inNum );
		virtual	Stream&			operator << ( const ContourElement& );

							// Read Operations
		virtual ExceptionCode	GetBytes( void*, int32_t& );
		virtual int32_t			ReadData( void*, int32_t );
				void			ReadBlock( void*, int32_t );
				int32_t			PeekData( void*, int32_t );
		friend	Stream&			operator >> ( Stream&, StringPtr outString );
		friend	Stream&			operator >> ( Stream&, char* outString );
		friend	Stream&			operator >> ( Stream&, int8_t &outNum );
		friend	Stream&			operator >> ( Stream&, uint8_t &outNum );
		friend	Stream&			operator >> ( Stream&, bool &outBool );
ACPLAPI	friend	Stream&			operator >> ( Stream&, UString& string );
		virtual	Stream&			operator >> ( String& outStr );
		virtual	Stream&			operator >> ( int16_t &outNum );
		virtual	Stream&			operator >> ( uint16_t &outNum );
		virtual	Stream&			operator >> ( int32_t &outNum );
		virtual	Stream&			operator >> ( uint32_t &outNum );
		virtual	Stream&			operator >> ( long &outNum );
		virtual	Stream&			operator >> ( unsigned long &outNum );
#if ACPL_WIN || ACPL_MAC
		virtual	Stream&			operator >> ( int64_t &outNum );
#endif
		virtual	Stream&			operator >> ( float &outNum );
		virtual	Stream&			operator >> ( double &outNum );
		virtual	Stream&			operator >> ( ContourElement& );
				void			Format( const char*, ... );
				void			WriteString( const char* );
		virtual	void			WriteUTF16String( const UString& inString );
		virtual	UString			ReadUTF16String();
				ExceptionCode	Copy( Stream& destStream );
	protected:
				int32_t			WritePString( ConstStringPtr inString );
				int32_t			ReadPString( StringPtr outString );
				int32_t			WriteCString( const char* inString );
				int32_t			ReadCString( char* outString );

				int64_t			mMarker;
				int64_t			mLength;
	};

	inline bool	Stream::AtEnd() const
	{
		return GetMarker() >= GetLength();
	}

	inline int32_t	Stream::WriteData( const void* inBuffer, int32_t inByteCount )
	{
		PutBytes(inBuffer, inByteCount);
		return inByteCount;
	}

	inline	Stream&	operator << ( Stream& str, bool inBool )
	{
		uint8_t	b = (uint8_t)inBool;
		str.WriteBlock(&b, sizeof(b));
		return str;
	}

	inline Stream&	operator << ( Stream& str, ConstStringPtr inString )	// Pascal string
	{
		str.WritePString(inString);
		return str;
	}

	inline Stream&	operator << ( Stream& str, const char * inString )
	{
		str.WriteCString(inString);
		return str;
	}

	inline Stream&	operator << ( Stream& str, int8_t inNum )
	{
		str.WriteBlock(&inNum, sizeof(inNum));
		return str;
	}

	inline Stream&	operator << ( Stream& str, uint8_t inNum )
	{
		str.WriteBlock(&inNum, sizeof(inNum));
		return str;
	}

		// Read Operations


	inline	Stream&	operator >> ( Stream& str, bool &outBool )
	{
		uint8_t	b;
		str.ReadBlock( &b, sizeof(b) );
		outBool = b != 0;
		return str;
	}

	inline Stream&	operator >> ( Stream& str, StringPtr outString )
	{
		str.ReadPString(outString);
		return str;
	}

	inline Stream&	operator >> ( Stream& str, char* outString )
	{
		str.ReadCString(outString);
		return str;
	}

	inline Stream&	operator >> ( Stream& str, int8_t &outNum )
	{
		str.ReadBlock(&outNum, sizeof(outNum));
		return str;
	}

	inline Stream&	operator >> ( Stream& str, uint8_t &outNum )
	{
		str.ReadBlock(&outNum, sizeof(outNum));
		return str;
	}

}}
