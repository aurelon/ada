/*
 * PreflightStream
 * Copyright (c) 1998-2012 Aurelon. All rights reserved.
 *
 * Class for preflightinh an ordered sequence of bytes
 */

#pragma once

#include "./Stream.h"

namespace aur { namespace ACPL {

	class ACPLAPI PreflightStream : public Stream
	{
	public:
						PreflightStream();
		ExceptionCode	PutBytes( const void*, int32_t& );
	private:
						PreflightStream( const PreflightStream& );
		void			operator=( const PreflightStream& );
	};

}}
