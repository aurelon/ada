// ===========================================================================
//	ResourceStream.h	1998-2012 Aurelon. All rights reserved.
// ===========================================================================
//
//	A Stream whose bytes are in a Handle block in memory

#pragma once

#include "./Stream.h"

#if ACPL_MAC

namespace aur { namespace ACPL {

	class FileSpec;

	class ACPLAPI ResourceStream : public Stream
	{
		ResFileRefNum			mRef;
		Handle					mResHandle;
		bool					mRelease;
	public:
								ResourceStream( const FileSpec&, ResType, short, bool );
								ResourceStream( const FileSpec&, ResType, const StringPtr );
								ResourceStream( Handle );
		virtual					~ResourceStream();

		virtual ExceptionCode	GetBytes( void*, int32_t& );
	};

}}

#endif
