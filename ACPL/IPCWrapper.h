#pragma once

#include "IPC.h"
#include "Multithreading.h"

namespace aur { namespace ACPL {
	
	class ACPLAPI IPCWrapper
	{
	public:
								IPCWrapper();
		virtual					~IPCWrapper();

		virtual bool			Connect( const IPC::ConnectData* connection );
		virtual bool			Send( IPC::Message* message );
		virtual bool			Disconnect();
		Signal<Stream*>*		ReceiveSignal();

		static  void			Init();
		static  void			Terminate();
		static	int32_t			ScanAndRegisterPort();
		static	void			UnregisterPort( const int32_t port );
		static	bool			IsPortAvailable( const int32_t port );
	private:
			    IPC*			mIpc;
	};
}}
