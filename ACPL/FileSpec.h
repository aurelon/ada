//**************************************************************************************
// Filename:	FileSpec.h
// Copyright 1998-2012 Aurelon. All rights reserved.
//
// Description:	Specifier for a file. A FSRef on Mac and a C string on Windows
//
//**************************************************************************************
// Revision History:
// zondag, 8 maart 1998 - Original
// zaterdag, 20 augustus 2011 - 64 bits
//**************************************************************************************

#pragma once

#include "Types.h"
#include "CString.h"
#include <time.h>
#if ACPL_LINUX
#	include <dirent.h>
#endif

namespace aur { namespace ACPL {

#if	ACPL_WIN
	typedef	time_t		TimeStamp;
#else
	typedef	uint32_t	TimeStamp;
#endif

#define	POSIX_PATH_LEN	1024

	typedef	struct	FileInfo
	{
		bool		isDir;
		bool		isLocked;
		bool		isVisible;
		char		extension[8];	//	this is always lower case
		TimeStamp	creationTime;
		TimeStamp	modificationTime;
		int64_t		dataForkSize;
#if ACPL_MAC
		int64_t		resourceForkSize;
		uint32_t	macType;
		uint32_t	macCreator;
#endif
	}	FileInfo;

	class ACPLAPI FileSpec
	{
#if ACPL_WIN
				wchar_t		mPath[POSIX_PATH_LEN];
		static	wchar_t		sWorkDisk[POSIX_PATH_LEN];
#elif ACPL_MAC
				FSRef		mParentRef;
				HFSUniStr255 mName;
				bool		mIsVolume;
        static	char		sWorkDisk[POSIX_PATH_LEN];
#elif ACPL_LINUX
				char		mPath[POSIX_PATH_LEN];
        static	char		sWorkDisk[POSIX_PATH_LEN];
#endif
#if ACPL_MAC || ACPL_LINUX
        static	bool		sUseSharedDataDir;
#endif
		static	String		sTempAppID;
		static	UString		sDataPath;
	public:
							FileSpec();
							FileSpec( const FileSpec& );
							FileSpec( const UniChar* );
#if ACPL_MAC
							FileSpec( const FSRef& );
#elif ACPL_WIN
							FileSpec( const char* );
							FileSpec( const wchar_t* );
#endif
			FileSpec&		operator=( const FileSpec& );

			ExceptionCode	Make( const char* path );
			ExceptionCode	Make( const UniChar* path );
			ExceptionCode	Make( const FileSpec& parentSpec, const char* path );
			ExceptionCode	Make( const FileSpec& parentSpec, const UniChar* path );
#if ACPL_MAC
			ExceptionCode	Make( const FSRef&, const char* );
#elif ACPL_WIN
			ExceptionCode	Make( const wchar_t* path );
			ExceptionCode	Make( const FileSpec& parentSpec, const wchar_t* path );
#endif
			ExceptionCode	MakeUnique( const FileSpec&, const char*, const char* );
			ExceptionCode	MakeUnique( const FileSpec&, const UniChar*, const UniChar* );
			ExceptionCode	MakeDataDirectory(const char* appSuite = NULL );
			ExceptionCode	MakeApplicationDirectory();
			ExceptionCode	MakeTempDirectory();

			ExceptionCode	Create( bool guestAccess = false, bool hidden = false ) const;
			ExceptionCode	CreateDirectory( bool guestAccess = false ) const;
			ExceptionCode	CreateTemporary( const char* = NULL, const char* = NULL );
			ExceptionCode	Delete() const;
			ExceptionCode	Rename( const char* );
			ExceptionCode	Rename( const UniChar* );
			ExceptionCode	Move( const FileSpec& );
			ExceptionCode	Copy( const FileSpec& ) const;
			UString			FileTitle() const;
			void			GetPOSIXName( char outName[256] ) const;
			void			GetPOSIXName( UniChar outName[256] ) const;
			UString			GetPOSIXName() const;
			void			GetPOSIXPath( char path[POSIX_PATH_LEN] ) const;
			void			GetPOSIXPath( UniChar path[POSIX_PATH_LEN] ) const;
			UString			GetPOSIXPath() const;
			bool			GetParentDirectory( FileSpec& ) const;
			FileSpec		ParentDirectory() const;
			void			Lock() const;
			void			Unlock() const;

			bool			Exists() const;
			bool			DirectoryExists() const;
			bool			GetInfo( FileInfo& ) const;
			UString			GetOwner() const;
			bool			operator==( const FileSpec& ) const;
			bool			operator!=( const FileSpec& ) const;

	static	const char*		PathSeparator();
	static	bool			SameDisk( const FileSpec&, const FileSpec& );
	static	UString			ValidName( const UString& );
#if ACPL_MAC
			ExceptionCode	GetFSRef( FSRef& ) const;
			short			OpenMacResourceFork( bool create, short ) const;
	static	const char*		WorkDisk();
	static	void			WorkDisk( const char* );
#elif ACPL_WIN
	inline	operator const wchar_t *() const { return mPath; }
	inline	operator const UniChar *() const { return (const UniChar*)mPath; }
	static	const wchar_t*	WorkDisk();
	static	void			WorkDisk( const wchar_t * );
#elif ACPL_LINUX
	inline	operator const char *() const { return mPath; }
	static	const char*		WorkDisk();
    static	void			WorkDisk( const char * );
#endif
#if ACPL_MAC || ACPL_LINUX
    static	void			UseSharedDir( bool );
#endif
	static	void			TempFileUID( const char * );
	static	void			SetDataDirectory( const UniChar * );
	private:
			void			AddGuestSecurityAttributes() const;					
	};

	class ACPLAPI DirectoryIterator
	{
		int32_t		mIndex;
		bool		mItemIsDir;
#if ACPL_MAC
		FSRef		mDirectory;
		UniChar		mFilter[256];
		bool		mFilterFromStart;
		FSIterator	mIterator;
#elif ACPL_WIN
		void*		mHandle;
		wchar_t		mSearchPath[POSIX_PATH_LEN];
		uint32_t	mBaseLength;
#elif ACPL_LINUX
		DIR*		mHandle;
		char		mFilter[256];
		char		mSearchPath[POSIX_PATH_LEN];
#endif
	public:
					DirectoryIterator( const FileSpec&, const char* );
					DirectoryIterator( const FileSpec&, const UString& );
					~DirectoryIterator();
		bool		GetNext( FileSpec& );
		int32_t		GetItemIndex() const;
		void		MoveToIndex( int32_t );
		bool		ItemIsDirectory() const;
		std::vector<FileSpec>	AllFiles( bool inclInvisible = false );
		std::vector<FileSpec>	AllDirectories( bool inclInvisible = false );
	};

	inline	bool	DirectoryIterator::ItemIsDirectory() const
	{
		return mItemIsDir;
	}

	class ACPLAPI FileRef
	{
	public:
		FileRef();
		FileRef( const FileRef& );
		FileRef( const FileSpec&, bool cleanUp = false );
		FileRef& operator=( const FileRef & );
		FileRef& operator=( const FileSpec& );
		~FileRef();

		operator const FileSpec&() const;
		const FileSpec *operator->() const;
		bool	operator==( const FileRef& ) const;
		bool	operator==( const FileSpec& ) const;
		bool	operator!=( const FileRef& ) const;
		bool	operator!=( const FileSpec& ) const;
	private:
		class Data
		{
        public:
            Data()
                : mRefCount( 1 )
                , mCleanUp( false )
                , mSpec()
            {
            }
		public:
			ACPL_ATOMIC	mRefCount;
			bool		mCleanUp;
			FileSpec	mSpec;
		};
				Data*	d;
		static	Data*	EmptyData();
		static	void	Dispose( Data * );
		static	Data*	New( const FileSpec&, bool );
	};

	inline	FileRef::FileRef() : d( EmptyData() )
	{ 
		ACPL_CLONE( d->mRefCount );
	}

	inline	FileRef::FileRef( const FileRef& inStr ) : d( inStr.d )
	{
		ACPL_CLONE( d->mRefCount );
	}

	inline	FileRef::~FileRef()
	{
		if( ACPL_DISPOSE( d->mRefCount ) == 0 )
			Dispose( d );
	}

	inline FileRef::operator const FileSpec&() const
	{
		return d->mSpec;
	}

	inline const FileSpec *FileRef::operator->() const
	{
		return &d->mSpec;
	}

	inline	bool	FileRef::operator==( const FileRef& inRef ) const
	{
		return d == inRef.d || d->mSpec == inRef.d->mSpec;
	}

	inline bool	FileRef::operator==( const FileSpec& inSpec ) const
	{
		return d->mSpec == inSpec;
	}

	inline	bool	FileRef::operator!=( const FileRef& inRef ) const
	{
		return d->mSpec != inRef.d->mSpec;
	}

	inline bool	FileRef::operator!=( const FileSpec& inSpec ) const
	{
		return d->mSpec != inSpec;
	}

}}
