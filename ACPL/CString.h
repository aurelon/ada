//**************************************************************************************
// Filename:	String.h
// Copyright (c) 1998-2012 Aurelon. All rights reserved.
//
// Description:
//
//**************************************************************************************
// Revision History:
// woensdag, 29 juli 1998 - Original
//**************************************************************************************

#pragma once

#include "Types.h"
#include <cstring>
#include <string>
#include <vector>
#include <atomic>

#define	ACPL_ATOMIC			std::atomic_uint_fast32_t
#define	ACPL_CLONE( r )		(++r)
#define	ACPL_DISPOSE( r )	(--r)

namespace aur { namespace ACPL {

	class	Stream;
	class	UString;
	class	MemoryStream;

	class ACPLAPI String
	{
	public:
							String();
							String( const String& );
							String( const char* );
							String( const uint8_t* );
							String( const char*, uint32_t );
							String( const std::string& );
							String( const MemoryStream& );
				String&		operator=( const String & );
				String&		operator=( const UString & );
				String&		operator=( const char * );
				String&		operator=( const unsigned char * );
				String&		operator=( const std::string& );
							~String();

				int			GetLength() const;
				bool		IsEmpty() const;
							operator const char*() const;
							operator std::string() const;
		inline	const char*	c_str() const { return d->mString; }
				char*		GetBuffer( uint32_t );
				void		ReleaseBuffer();
				String&		operator+=( const char* );
				String&		operator+=( const String& );
				String&		operator+=( const char );
				String&		operator+=( const std::string& );
				void		Delete( int32_t, int32_t );
				int32_t		Find( const char* ) const;
				bool		Find( const char*, int& ) const;
				int32_t		Replace( const char*, const char* );
		std::vector<String>	Split( char delimiter ) const;
				String		Left( int32_t ) const;
				String		Right( int32_t ) const;
#ifdef __APPLE__
				void		Format( const char*, ... ) __attribute__((format(printf, 2, 3)));
		static	String		sprintf( const char*, ... ) __attribute__((format(printf, 1, 2)));
#else
				void		Format( const char*, ... );
		static	String		sprintf( const char*, ... );
#endif
				bool		operator==( const String& ) const;
				bool 		operator==( const char* ) const;
				bool 		operator==( const std::string& ) const;
				bool		operator!=( const String& ) const;
				bool		operator!=( const char* ) const;
				bool		operator!=( const std::string& ) const;
				bool		operator<( const String& ) const;
				bool		operator>( const String& ) const;
				String		ToLower() const;
				String		ToUpper() const;
				void		MakeLower();
				void		MakeUpper();
				int			ToInt( int defVal = 0 ) const;
				uint32_t	ToUInt( uint32_t defVal = 0 ) const;
				int64_t		ToInt64( int64_t defVal = 0 ) const;
				float		ToFloat( float defVal = 0 ) const;
				double		ToDouble( double defVal = 0 ) const;
				bool		ToBool() const;
		static	String		Number( int );
		static	String		Number( uint32_t );
		static	String		Number( int64_t );
		static	String		Number( double, int prec=6 );
	private:
		class Data
		{
        public:
            Data();
		public:
            ACPL_ATOMIC	mRefCount;
            char		mString[256];
		};
				Data*		d;
        static	Data*       EmptyData();
		static	void		Dispose( Data * );
		static	Data*		New( size_t );
	};

	typedef std::vector<String> StringList;

    inline	String::String() : d( EmptyData() )
	{ 
		ACPL_CLONE( d->mRefCount );
	}

	inline	String::String( const String& inStr ) : d( inStr.d )
	{ 
		ACPL_CLONE( d->mRefCount );
	}

	inline	String::~String()
	{
		if( ACPL_DISPOSE( d->mRefCount ) == 0 )
			Dispose(d);
	}

	inline	int	String::GetLength() const
	{
		return int( ::strlen( d->mString ) );
	}

	inline	bool	String::IsEmpty() const
	{
		return d->mString[0] == 0;
	}

	inline	String::operator const char*() const
	{
		return d->mString;
	}

	inline	String::operator std::string() const
	{
		return std::string( d->mString );
	}

	inline void String::ReleaseBuffer()
	{
	}

	inline String&	String::operator+=( const String& inStr )
	{
		return operator+=( inStr.d->mString );
	}

	inline String&	String::operator+=( const std::string& inStr )
	{
		return operator+=( inStr.c_str() );
	}

	inline bool	String::operator==( const std::string& s ) const
	{
		return operator==(s.c_str());
	}

	inline bool	String::operator!=( const String& s ) const
	{
		return !operator==(s);
	}

	inline bool	String::operator!=( const char* s ) const
	{
		return !operator==(s);
	}

	inline bool	String::operator!=( const std::string& s ) const
	{
		return !operator==(s.c_str());
	}

	class ACPLAPI UString
	{
	public:
#if ACPL_LINUX
        static	void			SetLocalEncoding( const char* );
        static	const char*		GetLocalEncoding();
#else
		static	void			SetLocalEncoding( uint32_t );
		static	uint32_t		GetLocalEncoding();
#endif

								UString();
								UString( const UString& );
								UString( const UniChar* );
								UString( const std::wstring& );
								UString( const wchar_t* );
#if ACPL_MAC
								UString( CFStringRef );
#endif
								~UString();
				UString&		operator=( const UString& );
				UString&		operator=( const UniChar * );
				UString&		operator=( const std::wstring& );
				UString&		operator=( const wchar_t * );
				int				GetLength() const;
				bool			IsEmpty() const;
				int32_t 		Find( const UniChar character ) const;
				int32_t 		ReverseFind( const UniChar character ) const;
	inline						operator const UniChar*() const { return d->mString; }
								operator std::wstring() const;
#if ACPL_WIN
	inline		const wchar_t*	w_str() const { return (wchar_t*)d->mString; }
#else
	inline		const UString&	w_str() const { return *this; }
#endif
	static		UString			FromUtf8( const uint8_t* utf8 );
				uint32_t		ToUTF8( uint8_t* utf8 ) const;
	static		UString			FromUtf8( const String& );
				String			ToUTF8() const;
				bool			FromLocalEncoding( const String& );
				String			ToLocalEncoding() const;
	static		UString			FromAscii( const char* );
#if ACPL_MAC
				CFStringRef		CreateCFString() const;
#endif
				UniChar*		GetBuffer( uint32_t );
				void			ReleaseBuffer();
				UString&		operator+=( const UniChar* );
				UString&		operator+=( const UString& );
				UString&		operator+=( const char* );
				UString&		operator+=( const std::wstring& );
				UString&		operator+=( const wchar_t* );
				void			Delete( int32_t, int32_t );
				UString			Left( uint32_t ) const;
				UString			Right( uint32_t ) const;
				void			Insert( uint32_t pos, const UniChar* insertStr );
				int32_t			Find( const UniChar* ) const;
				int32_t			Find( const wchar_t* ) const;
				int32_t 		ReverseFind( const UniChar* ) const;
				int32_t 		ReverseFind( const wchar_t* ) const;
				int32_t			Replace( const UniChar*, const UniChar* );
		std::vector<UString>	Split( UniChar delimiter ) const;
				void			Format( const UniChar*, ... );
				void			Format( const wchar_t*, ... );
				bool			operator==( const UString& ) const;
				bool			operator!=( const UString& ) const;
				bool			operator==( const UniChar* ) const;
				bool			operator!=( const UniChar* ) const;
				bool			operator==( const std::wstring& ) const;
				bool			operator!=( const std::wstring& ) const;
				bool			operator==( const wchar_t* ) const;
				bool			operator!=( const wchar_t* ) const;
				bool			operator<( const UString& ) const;
				bool			operator>( const UString& ) const;
				UString			ToLower() const;
				UString			ToUpper() const;
				int				ToInt( int defVal = 0 ) const;
				uint32_t		ToUInt( uint32_t defVal = 0 ) const;
				int64_t			ToInt64( int64_t defVal = 0 ) const;
				float			ToFloat( float defVal = 0 ) const;
				double			ToDouble( double defVal = 0 ) const;
				bool			ToBool() const;
		static	UString			Number( int );
		static	UString			Number( uint32_t );
		static	UString			Number( int64_t );
		static	UString			Number( double, int prec=6 );
		static	int				Compare( const UString& a, const UString& b );
		static	int				ICompare( const UString& a, const UString& b );
	private:
		class Data
		{
        public:
            Data();
		public:
            ACPL_ATOMIC	mRefCount;
            UniChar*	mString;
		};
		Data*		d;
        static	Data*			EmptyData();
		static	void			Dispose( Data * );
		static	Data*			New( size_t );
	};

	typedef std::vector<UString> UStringList;

    inline	UString::UString() : d( EmptyData() )
	{ 
		ACPL_CLONE( d->mRefCount );
	}

	inline	UString::UString( const UString& inStr ) : d( inStr.d )
	{ 
		ACPL_CLONE( d->mRefCount );
	}

	inline	UString::~UString()
	{
		if( ACPL_DISPOSE( d->mRefCount ) == 0 )
			Dispose(d);
	}

	inline	bool	UString::IsEmpty() const
	{
		return d->mString[0] == 0;
	}

	inline void UString::ReleaseBuffer()
	{
	}

	inline UString&	UString::operator+=( const UString& inStr )
	{
		return operator+=( inStr.d->mString );
	}

	inline UString&	UString::operator+=( const std::wstring& inStr )
	{
		return operator+=( inStr.c_str() );
	}

	inline bool	UString::operator==( const std::wstring& s ) const
	{
		return operator==(s.c_str());
	}

	inline bool	UString::operator!=( const UString& s ) const
	{
		return !operator==(s);
	}

	inline bool	UString::operator!=( const UniChar* s ) const
	{
		return !operator==(s);
	}

	inline bool	UString::operator!=( const std::wstring& s ) const
	{
		return !operator==(s.c_str());
	}

	inline bool	UString::operator!=( const wchar_t* s ) const
	{
		return !operator==(s);
	}

	inline UString UString::FromUtf8( const String& inStr )
	{
		return FromUtf8( reinterpret_cast<const uint8_t *>( inStr.c_str() ) );
	}

	inline String&	String::operator=( const UString& inStr )
	{
		return operator=( inStr.ToUTF8() );
	}

	inline UString&	UString::operator=( const std::wstring& inStr )
	{
		return operator=( inStr.c_str() );
	}
	
#if ACPL_MAC
	class	StCFString
	{
		CFStringRef	mString;
	public:
		inline	StCFString( const UString& inStr ) : mString( inStr.CreateCFString() ) {}
		inline	~StCFString() { ::CFRelease( mString ); }
		inline	operator CFStringRef() const { return mString; }
	};
#endif
	
}}

inline bool operator==( const char *s1, const aur::ACPL::String &s2 )
{
	return s2.operator==(s1);
}

inline bool operator!=( const char *s1, const aur::ACPL::String &s2 )
{
	return !s2.operator==(s1);
}

inline bool	operator==( const std::string& s1, const aur::ACPL::String &s2 )
{
	return s2.operator==(s1.c_str());
}

inline bool	operator!=( const std::string& s1, const aur::ACPL::String &s2 )
{
	return !s2.operator==(s1.c_str());
}

inline const aur::ACPL::String operator+( const aur::ACPL::String &s1, const aur::ACPL::String &s2 )
{
	aur::ACPL::String t(s1);
	t += s2;
	return t;
}

inline const aur::ACPL::String operator+( const aur::ACPL::String &s1, const char *s2 )
{
	aur::ACPL::String t(s1);
	t += s2;
	return t;
}

inline const aur::ACPL::String operator+( const char *s1, const aur::ACPL::String &s2 )
{
	aur::ACPL::String t(s1);
	t += s2;
	return t;
}

inline const aur::ACPL::String operator+( const aur::ACPL::String &s1, const std::string& s2 )
{
	aur::ACPL::String t(s1);
	t += s2.c_str();
	return t;
}

inline const aur::ACPL::String operator+( const std::string& s1, const aur::ACPL::String &s2 )
{
	aur::ACPL::String t(s1.c_str());
	t += s2;
	return t;
}

inline bool	operator==( const aur::UniChar* s1, const aur::ACPL::UString& s2 )
{
	return s2.operator==(s1);
}

inline bool	operator!=( const aur::UniChar* s1, const aur::ACPL::UString& s2 )
{
	return !s2.operator==(s1);
}

inline bool	operator==( const wchar_t* s1, const aur::ACPL::UString& s2 )
{
	return s2.operator==(s1);
}

inline bool	operator!=( const wchar_t* s1, const aur::ACPL::UString& s2 )
{
	return !s2.operator==(s1);
}

inline bool	operator==( const std::wstring& s1, const aur::ACPL::UString& s2 )
{
	return s2.operator==(s1.c_str());
}

inline bool	operator!=( const std::wstring& s1, const aur::ACPL::UString& s2 )
{
	return !s2.operator==(s1.c_str());
}

inline const aur::ACPL::UString operator+( const aur::ACPL::UString &s1, const aur::ACPL::UString &s2 )
{
	aur::ACPL::UString t(s1);
	t += s2;
	return t;
}

inline const aur::ACPL::UString operator+( const aur::ACPL::UString &s1, const char *s2 )
{
	aur::ACPL::UString t(s1);
	t += s2;
	return t;
}

inline const aur::ACPL::UString operator+( const char *s1, const aur::ACPL::UString &s2 )
{
	return aur::ACPL::UString::FromAscii(s1) + s2;
}

inline const aur::ACPL::UString operator+( const aur::ACPL::UString &s1, const wchar_t *s2 )
{
	aur::ACPL::UString t(s1);
	t += s2;
	return t;
}

inline const aur::ACPL::UString operator+( const wchar_t *s1, const aur::ACPL::UString &s2 )
{
	aur::ACPL::UString t(s1);
	t += s2;
	return t;
}

inline const aur::ACPL::UString operator+( const aur::ACPL::UString &s1, const std::wstring& s2 )
{
	aur::ACPL::UString t(s1);
	t += s2.c_str();
	return t;
}

inline const aur::ACPL::UString operator+( const std::wstring& s1, const aur::ACPL::UString &s2 )
{
	aur::ACPL::UString t(s1.c_str());
	t += s2;
	return t;
}
