#pragma once

#include "CString.h"
#include "GUID.h"
#include <vector>

namespace aur { namespace ACPL {

	class	FileSpec;
	class	Stream;
	class	MemoryStream;

	typedef	enum XMLResult
	{
		eXMLNoError,
		eXMLNotValid,
		eXMLNotFound,
		eXMLInvalidTag
	} XMLResult;

	class ACPLAPI XML
	{
	public:
		class	Node;
		class ACPLAPI Element
		{
			friend class Node;
		public:
							Element( const char* name, const char* value ); 
							Element( const char* name, int32_t value );
							Element( const char* name, int64_t value );
							Element( const char* name, uint32_t value );
							Element( const char* name, float value );
							Element( const char* name, double value );
							Element( const char* name, bool value );
							Element( const char* name, MemoryStream& value );
							Element( const char* name, const String& value );
							Element( const char* name, const UString& value );
			virtual			~Element();

			String&			operator+=( const char* inStr );
			Element&		operator=( const Element& );
			bool			operator==( const Element& );
			operator const char*() const;
			Element*		Duplicate() const;

			Node*			GetParentNode() const;

			float			ToFloat( float defaultV = 0 ) const;
			double			ToDouble( double defaultV = 0 ) const;
			int32_t			ToInt( int32_t defaultV = 0 ) const;
			int64_t			ToInt64( int64_t defaultV = 0 ) const;
			uint32_t		ToUInt( uint32_t defaultV = 0 ) const;
			bool			ToBool( bool defaultV = false ) const; 
			const String&	ToString() const;
			UString			ToUString() const;
			uint32_t		ToBinary( uint8_t* data ) const;
			void			ToBinary( MemoryStream& ) const;
			bool			ToBinaryFromBase64( MemoryStream& ) const;
			Guid			ToGUID() const;

			void			SetValue( const char* v );
			void			SetValue( int32_t v );
			void			SetValue( int64_t v );
			void			SetValue( uint32_t v );
			void			SetValue( float v );
			void			SetValue( double v );
			void			SetValue( bool v );
			void			SetValue( uint32_t dataLen, const uint8_t* data );
			void			SetValue( MemoryStream& data );
			void			SetValueBase64( uint32_t dataLen, const uint8_t* data );
			void			SetValue( const String& v );
			void			SetValue( const UString& v );
			void			Format( const char* format, ... );

			const String&	Name() const;
			void			SetName( const char* name );
			bool			IsEmpty() const;

		protected:
			String			mValue;
			String			mName;
			Node*			mParentNode;

			virtual void	WriteToFile( Stream&, const char* );
		};

		typedef std::vector<Element*> ElementsList;
		typedef std::vector<Node*> NodesList;

		class ACPLAPI Node : public Element
		{
			friend class XML;

		public:
			typedef std::ptrdiff_t difference_type;
			typedef size_t size_type;
			typedef Node value_type;
			typedef Node* pointer;
			typedef const Node* const_pointer;
			typedef Node& reference;
			typedef const Node& const_reference;
			
			struct const_iterator;
			
			struct iterator {
				// Keeps a reference to the container:
				Node* ref;
				size_t pos;
				
				// Set iterator to next() state:
				void next()  { ++pos;  }
				// Optional function for reverse iteration:
				void prev() { --pos; }
				// Initialize iterator to first state:
				void begin() { pos = 0; }
				// Initialize iterator to end state:
				void end()   { pos = ref == NULL || ref->mChildNodes == NULL ? 0 : ref->mChildNodes->size(); }
				// Returns current `value`
				reference get() { return *ref->mChildNodes->at(pos); }

			public:
				static iterator begin(Node* ref) {
					iterator it(ref);
					it.begin();
					return it;
				}
				static iterator end(Node* ref) {
					iterator it(ref);
					it.end();
					return it;
				}
				
			protected:
				iterator(Node* ref) : ref(ref) {}
				
			public:
				// Note: Instances build with this constructor should
				// be used only after copy-assigning from other iterator!
				iterator() {}
				
			public:
				reference operator*()  { return  get(); }
				pointer operator->() { return &get(); }
				iterator& operator++() { next(); return *this; }
				iterator operator++(int) { iterator temp(*this); next(); return temp; }
				iterator& operator--() { prev(); return *this; }
				iterator operator--(int) { iterator temp(*this); prev(); return temp; }
				bool operator!=(const iterator& other) const { return ref != other.ref || pos != other.pos; }
				bool operator==(const iterator& other) const { return ref == other.ref && pos == other.pos; }
				
				friend struct const_iterator;
				
				// Comparisons between const and normal iterators:
				bool operator!=(const const_iterator& other) const { return ref != other.ref || pos != other.pos; }
				bool operator==(const const_iterator& other) const { return ref == other.ref && pos == other.pos; }
			};

			// Mutable Iterator:
			iterator begin() { return iterator::begin(this); }
			iterator end() { return iterator::end(this); }

			struct const_iterator {
				// Keeps a reference to the container:
				const Node* ref;
				size_t pos;

				// Set iterator to next() state:
				void next()  { ++pos;  }
				// Optional function for reverse iteration:
				void prev() { --pos; }
				// Initialize iterator to first state:
				void begin() { pos = 0; }
				// Initialize iterator to end state:
				void end()   { pos = ref == NULL || ref->mChildNodes == NULL ? 0 : ref->mChildNodes->size(); }
				// Returns current `value`
				const_reference get() { return *ref->mChildNodes->at(pos); }
				
			public:
				static const_iterator begin(const Node* ref) {
					const_iterator it(ref);
					it.begin();
					return it;
				}
				static const_iterator end(const Node* ref) {
					const_iterator it(ref);
					it.end();
					return it;
				}
				
			protected:
				const_iterator(const Node* ref) : ref(ref) {}
				
			public:
				// Note: Instances build with this constructor should
				// be used only after copy-assigning from other iterator!
				const_iterator() {}
				
				// To make possible copy-construct non-const iterators:
				const_iterator(const iterator& other) : ref(other.ref) {
					pos = other.pos;
				}
				
			public:
				const_reference operator*()  { return get(); }
				const_pointer operator->() { return &get(); }
				const_iterator& operator++() { next(); return *this; }
				const_iterator operator++(int) { const_iterator temp(*this); next(); return temp; }
				const_iterator& operator--() { prev(); return *this; }
				const_iterator operator--(int) { const_iterator temp(*this); prev(); return temp; }
				bool operator!=(const const_iterator& other) const { return ref != other.ref || pos != other.pos; }
				bool operator==(const const_iterator& other) const { return ref == other.ref && pos == other.pos; }
				const_iterator& operator=(const iterator& other) {
					ref = other.ref;
					pos = other.pos;
					return *this;
				}
				
				friend struct iterator;
				
				// Comparisons between const and normal iterators:
				bool operator!=(const iterator& other) const { return ref != other.ref || pos != other.pos; }
				bool operator==(const iterator& other) const { return ref == other.ref && pos == other.pos; }
			};

			// Const Iterator:
			const_iterator begin() const { return const_iterator::begin(this); }
			const_iterator end() const { return const_iterator::end(this); }

		public:
							Node( const char* name = NULL );
							Node( const char* name, const char* value ); 
							Node( const char* name, int32_t value );
							Node( const char* name, int64_t value );
							Node( const char* name, uint32_t value );
							Node( const char* name, float value );
							Node( const char* name, double value );
							Node( const char* name, bool value );
							Node( const char* name, MemoryStream& value );
							Node( const char* name, const String& value );
							Node( const char* name, const UString& value );
			virtual			~Node();

			bool			HasName() const;
			bool			HasAttributes() const;
			bool			HasChildren() const;

			uint32_t		GetNrOfChildren() const;
			uint32_t		AttributesCount() const;

			Element*		GetAttributeByIndex( uint32_t index ) const;
			Element*		GetAttributeByName( const char* name ) const;
			Element*		GetElementByXPath( const char* name );

			Node*			GetNodeByIndex( uint32_t index ) const;
			Node*			GetNodeByName( const char* ) const;
			Node*			GetNodeByXPath( const char* );
			
			#define GetElementByIndex GetNodeByIndex
			#define GetElementByName GetNodeByName

			void			AddAttribute( Element* element );
			void			AddAttribute( const char* name, const char* value );
			void			AddAttribute( const char* name, int32_t value );
			void			AddAttribute( const char* name, uint32_t value );
			void			AddAttribute( const char* name, float value );
			void			AddAttribute( const char* name, double value );
			void			AddAttribute( const char* name, bool value );
			void			AddAttribute( const char* name, const String& value );
			void			AddAttribute( const char* name, const UString& value );
			bool			DeleteAttribute( const char* name );

			void			AddChildNode( Node*, uint32_t atPosition = 0xFFFFFFFFU );
			XML::Node*		AddChildNode( const char* name, uint32_t atPosition = 0xFFFFFFFFU );
			bool			ReplaceChildNode( Node* );
			bool			DeleteChildNode( const Node* );
			bool			DeleteChildNodeByName( const char* );
			bool			DeleteChildNodeByIndex( uint32_t );
			bool			DetachChildNode( const Node* );
			Node*			Duplicate() const;

			void			SetElementValue( const char* label, int32_t value );
			void			SetElementValue( const char* label, int64_t value );
			void			SetElementValue( const char* label, uint32_t value );
			void			SetElementValue( const char* label, bool value );
			void			SetElementValue( const char* label, float value );
			void			SetElementValue( const char* label, double value );
			void			SetElementValue( const char* label, const char* str );
			void			SetElementValue( const char* label, const String& str );
			void			SetElementValue( const char* label, const UString& str );
			void			SetElementValue( const char* label, uint32_t dataLen, const uint8_t* data );
			void			SetElementValue( const char* label, MemoryStream& data );

			bool			GetElementValue( const char* label, int32_t& value ) const;
			bool			GetElementValue( const char* label, int64_t& value ) const;
			bool			GetElementValue( const char* label, uint32_t& value ) const;
			bool			GetElementValue( const char* label, bool& value ) const;
			bool			GetElementValue( const char* label, float& value ) const;
			bool			GetElementValue( const char* label, double& value ) const;
			bool			GetElementValue( const char* label, char* str ) const;
			bool			GetElementValue( const char* label, String& str ) const;
			bool			GetElementValue( const char* label, UString& str ) const;
			bool			GetElementValue( const char* label, uint32_t& dataLen, uint8_t* data ) const;
#if debugging
			/*!
			 DbgString
			 \ingroup	XML

			 Returns the string representation of the node and all its attributes and children under the  
			 form of a const char*. The method is deliberately leaking to avoid memory allocation problems with the Windows debugger.
			 Usage
			 Mac:
			 -	Set a breakpoint and type in the console: printf "%s", node->DbgString(). The contents will be displayed in the console.
			 Win:
			 -	Limitation: DbgString cannot be called from the application context since it is defined in a dll. 
			 -	Add a global function called PrintNode( ACPL::XML::Node* ); in stdafx which calls the node method for us and dumps everything in the output console.
			 The method should contain the following code: OutputDebugString( node->DbgString() );
			 -	Usage: set a breakpoint, add a watch expression: node->PrintNode(). To get updates, use the refresh button. The XML contents are dumped in the output window.
			 */
			const char *	DbgString();
#if ACPL_MAC
			/*!
			 DumpNode
			 \ingroup	XML

			 Mac convenience XML debugging method.
			 The debugger console command is simple: call node->DumpNode()
			 The entire node will be displayed formatted in the console output.
			 */
			void			DumpNode();
#endif
#endif
		protected:
			ElementsList*	mAttributes;
			NodesList*		mChildNodes;

			void			ReadNodeData( Stream& );
			XMLResult		ParseContent( Stream& );
			XMLResult		ParseTag( const String& );
			virtual void	WriteToFile( Stream&, const char* );
		};

					XML( const char* name = NULL );
					~XML();
		XMLResult 	Load( const FileSpec&, bool validate = true );
		XMLResult	Load( Stream&, bool validate = true );
		XMLResult	Load( const char*, bool validate = true );
		void 		Save( const FileSpec& );
		void 		Save( Stream& );
		String		Save();
		void		Empty();
		void		Initialize( const char* name );
		bool		IsXMP() const;
		void		SetXMP( bool xmp );

		Node*		root;
		Node*		GetNodeByXPath( const char* );
		Element*	GetElementByXPath( const char* );
		const char*	GetValueByXPath( const char* );

	private:
		bool		mIsXMP;
		XMLResult 	ValidateXML( Stream& );
		void 		SkipDTD( Stream& );
	};

	inline	bool	XML::IsXMP() const
	{
		return mIsXMP;
	}

	inline	void	XML::SetXMP( bool xmp )
	{
		mIsXMP = xmp;
	}

#if ACPL_MAC
#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wtautological-compare"
#endif

	inline	bool	XML::Element::IsEmpty() const
	{
		if( this == NULL )
			return true;
		return mValue.IsEmpty();
	}

#if ACPL_MAC
#	pragma clang diagnostic pop
#endif

	inline	void	XML::Element::SetValue( const char* v )
	{
		mValue = v;
	}

	inline	void	XML::Element::SetValue( int32_t v )
	{
		mValue = String::Number( v );
	}

	inline	void	XML::Element::SetValue( int64_t v )
	{
		mValue = String::Number( v );
	}

	inline	void	XML::Element::SetValue( uint32_t v )
	{
		mValue = String::Number( v );
	}

	inline void		XML::Element::SetValue( bool v )
	{
		mValue = v ? "true" : "false";
	}

	inline XML::Element&	XML::Element::operator=( const Element& inVal )
	{
		mValue = inVal.mValue;
		mName = inVal.mName;
		return *this;
	}

	inline bool	XML::Element::operator==( const Element& other )
	{
		return mName == other.mName && mValue == other.mValue;
	}

	inline const String& XML::Element::Name() const
	{
		return mName;
	}

	inline void	XML::Element::SetName( const char* name)
	{
		mName = name;
	}

	inline	XML::Node* XML::Element::GetParentNode() const
	{
		return mParentNode;
	}

	inline	bool	XML::Node::HasName() const
	{
		return mName.GetLength() > 0;
	}

#if ACPL_MAC
#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wtautological-compare"
#endif

	inline	bool	XML::Node::HasAttributes() const
	{
		return this != NULL && mAttributes && !mAttributes->empty();
	}

	inline	bool	XML::Node::HasChildren() const
	{
		return this != NULL && mChildNodes && !mChildNodes->empty();
	}

#if ACPL_MAC
#	pragma clang diagnostic pop
#endif

	inline	void	XML::Node::AddAttribute( const char* name, const char* value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, int32_t value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, uint32_t value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, float value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, double value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, bool value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, const String& value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline	void	XML::Node::AddAttribute( const char* name, const UString& value )
	{
		AddAttribute( new Element( name, value ) );
	}

	inline XML::Node*	XML::Node::GetNodeByXPath( const char* path )
	{
		return dynamic_cast<Node*>( GetElementByXPath(path) );
	}

}}
