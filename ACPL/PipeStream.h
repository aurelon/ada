// ===========================================================================
//	PipeStream.h			   2003-2012 Aurelon. All rights reserved.
// ===========================================================================
//
//	A Stream whose bytes are shared and piped to another stream

#pragma once

#include "./Stream.h"

namespace aur { namespace ACPL {

	class ACPLAPI PipeStream : public Stream
	{
		class PipeData;
		PipeData*				mDataOut;
		PipeData*				mDataIn;
	public:
								PipeStream( PipeStream*, uint32_t = 8192 );
		virtual					~PipeStream();

		virtual ExceptionCode	PutBytes( const void*, int32_t& );
		virtual ExceptionCode	GetBytes( void*, int32_t& );
		virtual int64_t			GetLength() const;

				bool			EndOfStream() const;
				int32_t			GetNonBlockingOutSize( uint32_t = 0 );
				void			Flush();
				void			Abort();
	};

}}
