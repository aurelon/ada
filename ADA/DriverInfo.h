#pragma once

#include <ACPL/Types.h>
#include <vector>

namespace aur {

	namespace ACPL {
		class	XML;
		class	String;
		class	UString;
		class	Stream;
		class	FileSpec;
		class	PipeStream;
	}

	class Imposition;

	namespace ADA {

	typedef uint32_t MarkType;

#define	SDK_VERSION		380

#ifndef inch
#	define inch		* 18.0f
#endif
#ifndef	mm
#	define mm		* ( 18.0f / 25.4f )
#endif

#define kMaxSheetLenght 20000 mm

	/*! \mainpage PrintFactory Driver SDK

		\section intro_sec Introduction

		The PrintFactory Driver SDK the possibility to create drivers for PrintFactory RIP
		based product and/or use driver compatible with PrintFactory RIP based products.

		There are 4 main modules in the API:
		- DLL exports : Main access routines to query the driver and/or to create a driver object (instance).
		- Instance methods : Explanation of methods available in the instance object (handle the device). For each instance of the device connected an driver instance is created.
		- Session methods : Explanation of methods available in the session object (to drive the device). For each job a session object is created by the instance. Only one session can be active per instance.
		- Driver information : Explanation of the data retrieved by the Info query of the instance object.
	*/

	/*!
		\defgroup	DLL_API		Driver DLL exported
	*/
	/*!
		\defgroup	INST_Methods	Instance methods
	*/
	/*!
		\defgroup	DRV_Methods	Session methods

		The methods described in the DriverMethods are to be called using one of the following calling orders.
		Driver rely on this.

		The calling order (Typical print only):

		- Initialize
		- JobStart
		- for( every layer in every page )
			- PageStart
			- RasterStart
			- for( every plane in every line )
				- ProcessRawBuffer
			- RasterEnd
			- PageEnd
		- JobEnd
		- Terminate

		The calling order (Typicall cutting only):

		- Initialize
		- JobStart
		- for( every page )
			- RegistrationMarks (if present)
			- PageStart
			- VectorStart
			- Mix of calls below to build contours
				- SetTool
				- Move
				- Line
				- Vertex
				- PenUp
				- PenDown
				- CurveStart
				- CurveEnd
			- VectorEnd
			- PageEnd
			- Transport
		- JobEnd
		- Terminate

		The calling order (Print and cut):

		- Initialize
		- JobStart
		- for( every layer in every page )
			- RegistrationMarks (if present)
			- PageStart
			- RasterStart
			- for( every plane in every line )
				- ProcessRawBuffer
			- RasterEnd
			- VectorStart
			- Mix of calls below to build contours
				- SetTool
				- Move
				- Line
				- Vertex
				- PenUp
				- PenDown
				- CurveStart
				- CurveEnd
			- VectorEnd
			- PageEnd
			- Transport
		- JobEnd
		- Terminate

	*/
	/*!
		\defgroup	DRV_Info	Driver information
	*/
	/*!
		Halftone spot shape
		\ingroup	DRV_Info

		The types of spots that are used by standard drivers. Additional
		spot types can be defined based on PostScript function.
	*/
	typedef enum SpotType
	{
		eAdobeRound = 0,	//!<	Euclidean round
		eAdobeEllipse = 1,	//!<	Euclidean ellipse
		eDiamond = 2,
		eSquare = 3,
		eSimpleRound = 4,
		eEllipseA = 5,
		eEllipseB = 6,
		eEllipseC = 7,
		eCosine = 8,
		eLine = 9,
		eCross = 10,
		eRhomboid = 11,
		eEllipse2 = 12,
		eLineX = 13,
		eLineY = 14,
		eDoubleDot = 15,
		eInvertedDoubleDot = 16,
		eSimpleDot = 17,
		eInvertedEllipseA = 18,
		eInvertedEllipseC = 19
	}	SpotType;

	/*!
		Screening type requested from the RIP
		\ingroup	DRV_Info
	*/
	typedef enum ScreenType
	{
		eNotDefined = 0,		//!<	No screening type prefered
		eErrorDiffusion = 1,	//!<	Any stochastic (multi level) screening (PrintFactory CED)
		eFMScreen = 2,			//!<	FM mask based screening
		eAMScreen = 3,			//!<	Halftones (Shape optimized)
		eContone8 = 4,			//!<	No screening, 8-bit contone data
		eFloydSteinberg = 5,	//!<	Floyd-Steinberg error diffusion
		eChromaticED = 6,		//!<	Chromatic correct error diffusion, only avalable for CMYK
		eContone16 = 7			//!<	No screening, 16-bit contone data
	}	ScreenType;

	typedef enum USBFilterType
	{
		eByIEEEE = 0,
		eByModelName = 1
	}	USBFilterType;

	typedef enum EmbeddedProfileUse
	{
		eUseEmbeddedNone = 0,
		eUseEmbeddedProfileIntent = 1,
		eUseEmbeddedProfile = 2
	}	EmbeddedProfileUse;

	/*!
		Channel type of the ink
		\ingroup	DRV_Info
	*/
	typedef enum ChannelType
	{
		eProcess,				//!<	Process ink, use for profiling
		eSpot,					//!<	Spot color, do not profile
		eSpecial,				//!<	White ink or varnish
		eDuplicate				//!<Duplicate of other channel
	}	ChannelType;

	/*!
		Media size description
		\ingroup	DRV_Info

		Describes the media size and orientation
	*/
	class	TMediaSize
	{
	public:
		enum EMediaType{
			eLogical,		// Used in nesting and other logical operations.
			ePhysical		// Printed size.
		};

		char	name[32];		//!<	Name of the media size
		bool	transverse;		//!<	True when the media is fed rotated in the machine. The RIP must provide rotated data.
		bool	roll;			//!<	True when the media size is a roll, False when it is a sheet.
		float	width;			//!<	Physical width in AOI units
		float	length;			//!<	Physical length/height in AOI units
		float	left;			//!<	Left non-imageable margin
		float	right;			//!<	Right non-imageable margin
		float	top;			//!<	Top non-imageable margin
		float	bottom;			//!<	Bottom non-imageable margin

		inline float		GetLength				( EMediaType which = eLogical ) const;
		inline float		GetWidth				( EMediaType which = eLogical ) const;
		inline float		GetImageableLength		( EMediaType which = eLogical ) const;
		inline float		GetImageableWidth		( EMediaType which = eLogical ) const;

		inline float		GetLeftMargin			( EMediaType which = eLogical ) const;
		inline float		GetRightMargin			( EMediaType which = eLogical ) const;
		inline float		GetTopMargin			( EMediaType which = eLogical ) const;
		inline float		GetBottomMargin			( EMediaType which = eLogical ) const;
	};

	inline float TMediaSize::GetLength( EMediaType which ) const
	{
		return which == ePhysical && transverse ? width : length;
	}

	inline float TMediaSize::GetWidth( EMediaType which ) const
	{
		return which == ePhysical && transverse ? length : width;
	}

	inline float TMediaSize::GetImageableLength( EMediaType which ) const
	{
		return which == ePhysical && transverse ? width - left - right : length - top - bottom;
	}

	inline float TMediaSize::GetImageableWidth( EMediaType which ) const
	{
		return which == ePhysical && transverse ? length - top - bottom : width - left - right;
	}

	inline float TMediaSize::GetLeftMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? bottom : left;
	}

	inline float TMediaSize::GetRightMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? top : right;
	}

	inline float TMediaSize::GetTopMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? left : top;
	}

	inline float TMediaSize::GetBottomMargin( EMediaType which ) const
	{
		return which == ePhysical && transverse ? right : bottom;
	}

	/*!
		Tray size description
		\ingroup	DRV_Info

		Describes the trays and the maximum size it can accept.
	*/
	class	TTray
	{
	public:
		char	name[32];		//!<	Name of the tray
		bool	roll;			//!<	True when the tray is a roll, False when it is sheet fed or a cassette.
		float	width;			//!<	Maximum physical width in AOI units of media it accepts.
		float	length;			//!<	Maximum physical length/height in AOI units of media it accepts.
		float	left;			//!<	Left non-imageable margin
		float	right;			//!<	Right non-imageable margin
		float	top;			//!<	Top non-imageable margin
		float	bottom;			//!<	Bottom non-imageable margin
	};

	/*!
		Ink definition
		\ingroup	DRV_Info

		Describes the ink of the channel and it properties.
	*/
	class	TInk
	{
	public:
		int8_t		color[3];			//!<	CIELab color to be used when generating previews without access to an ICC profile
		char		name[32];			//!<	Name of the ink. Prepended with the keyword "Light" or "Medium" to indicate the tone of the ink color.
		ScreenType	screening;			//!<	Prefered screening type for this ink.
		uint8_t		blueNoiseFactor;	//!<	Multiplication factor to apply on the blue noise of error diffusion or FM screening. Default 0. 0 is equal to a multiplication factor of 1.
		float		halftoneAngle;		//!<	Default halftone angles to be used when the screening is set to halftone.
		float		halftoneFrequency;	//!<	Default halftone frequency to be used when the screening is set to halftone.
		SpotType	halftoneShape;		//!<	Default halftone shape to be used when the screening is set to halftone.
		float		maxDotAmount;		//!<	Maximum amount of dots for all but large dot to be used for the screening. Range [0-1].
		ChannelType	type;				//!<	Channel type of this ink. The ink can either Process, Spot or Special.
		uint32_t	dotCount;			//!<	Number of levels for this channel (ink) including 0. Thus 1 drop size means 2 levels and 2 drop sizes means 3 levels.
		float		dotSizes[8];		//!<	Relative dot weights normalize to 1. A dotweight of 1 means full coverage. A dotweight of 0.5 means 50% coverage.
										//!<	Dotsizes larger of 1 are allowed and impose a screening based ink limit on that dot.
										//!<
										//!<	The number of dot sizes is dotCount-1
		float		picoliter[8];		//!<	Number of picoliter of the dot. The number of dots is dotCount-1. Only valid when Caps::estimateInfo is set.
	};

	/*!
		Driver capabilities
		\ingroup	DRV_Info

		Describes the capabilities of the driver.
	*/
	class Caps
	{
	public:
		bool	print;
		bool	cut;
		bool	flatbed;
		bool	highlevel;
		bool	additiveColors;
		bool	estimateInfo;
		bool	barcodeScanner;
		bool	mediaQuery;
		bool	deviceStatus;
		bool	deviceStatusWhilePrinting;
		bool	jobStatus;
		bool	ripOnDemand;
		bool	hasConsole;
		bool	doubleSided;
		bool	sideBAsLayer;
		bool	needPreviewAtJobStart;
		bool	needUnimagedArea;
		bool	serviceUI;
		bool	needsBlocker;
	};
	/*!
		\var	bool	Caps::print
				Flag indicate that this driver has printing capabilities.
	*/
	/*!
		\var	bool	Caps::cut
				Flag indicate that this driver has (contour) cutting capabilities.
	*/
	/*!
		\var	bool	Caps::flatbed
				Flag indicate that this device is a flatbed printer or plotter.
	*/
	/*!
		\var	bool	Caps::highlevel
				Flag indicate that this driver supports high level vector graphics
				expressed in real coordinates and supporting bezier curves.
	*/
	/*!
		\var	bool	Caps::additiveColors
				Flag indicate that this driver (using the current settings) has
				additive colors instead of substractive inks.
	*/
	/*!
		\var	bool	Caps::estimateInfo
				Flag indicate that this driver supplies ink estimation and time
				estimation information. See TInk::picoliter and sqUnitsPerHour
	*/
	/*!
		\var	bool	Caps::barcodeScanner
				Flag indicate that this device has a build-in barcode scanner for
				job recognition.
	*/
	/*!
		\var	bool	Caps::mediaQuery
				Flag indicate that this device supports measuring of loaded media
				and optionally provides information on media type.
	*/
	/*!
		\var	bool	Caps::deviceStatus
				Flag indicate that this device supports feedback on device status.
				This might include loaded media size and type and ink levels.
	*/
	/*!
		\var	bool	Caps::jobStatus
				Flag indicate that this device supports status information based on
				the job its GUID.
	*/
	/*!
		\var	bool	Caps::hasConsole
				Flag indicate that this device has a DFE and jobs will first appear there
				and require a printer operator to print the job.
	 */
	/*!
		\var	bool	Caps::doubleSided
				Flag indicate that this device has double sided printing capabilities with
				automatic loading the B-side after the A-side has been printed.
	 */
	/*!
		\var	bool	Caps::sideBAsLayer
				Flag indicate that this device has double sided printing capabilities with
				automatic placing the B-side as a layer in the same print size A-side has been
	 			printed. Where the B-side is placed as first layer and A-side as second layer.
	 			The B-side is expected to be delivered mirrored so it is readable from the
	 			reverse side of the print.
	 */
	/*!
		\var	bool	Caps::needPreviewAtJobStart
				Flag indicate that this driver requires a preview in the job XML before
				JobStart is called.
	 */
	/*!
		\var	bool	Caps::needUnimagedArea
				Flag indicate that this device no positioning capabilities and thus the x/y
				position in PageStart and RasterStart are ignored. This means that the RIP
				has to image also the non-imaged area in order to get correct positioning.
	 */
	/*!
		\var	bool	Caps::needsBlocker
				Flag indicating the need for a blocker page between A & B page. This
				printing mode is a sandwich mode, usually 5 layer: C-W-Blocker-W-C.
				The page side passed to PageStart for the blocker page is X.
	 */

	/*!
		Connection options
		\ingroup	DRV_Info

	 	Describes the connections supported by the driver.
	*/
	class Conns
	{
	public:
		bool	parallel;
		bool	tcpip;
		bool	serial;
		bool	usb;
		bool	custom;
		bool	folder;
		bool	hplx;
		bool	autoIP;
	};
	/*!
		\var	bool	Conns::parallel
				Flag indicate that this driver supports Centronics connections.

				The deviceURL needs to conform to the format "par://[port name]".
	*/
	/*!
		\var	bool	Conns::tcpip
				Flag indicate that this driver supports TCP/IP connection. The default
				TCP port is defined by tcpPort.

				The deviceURL needs to conform to the format "tcp://[host]:[port]" or
				"raw://[host]:[port]" for RTelnet / Raw protocol. When no port is defined
				9100 is assumed.
				The format "lpr://[host]:[port]" invokes the LPR/LPD protocol and port 515
				is assumed when ommited.
				The format "http://[host]:[port]" invoked a Raw protocol on port 80 when ommitted.
				The [host] might be an IP addres in the form A:B:C:D or a DNS name.
		\sa		tcpPort
	*/
	/*!
		\var	bool	Conns::serial
				Flag indicate that this driver supports serial connection. The default
				serial settings are defined by serBaudRate and serFlow.
				device that matches the pattern.

			The deviceURL needs to conform to the format "ser://[port name]:[baud],[bits],[parity],[handshake]".
				For valid values of handshake see serFlow.
		\sa		serBaudRate, serFlow
	*/
	/*!
		\var	bool	Conns::usb
				Flag indicate that this driver supports USB connection. The default
				USB IEEE filter and class or GUID. On Windows the usbGUID is used to
				located the driver. On MacOS X the usbFilter is used to find an USB
				device that matches the pattern.

				The deviceURL needs to conform on Windows to the format "usb://USB[port index]".
				On MacOS X the format needs to be "usb://[port name]:[model],[class]:[filter]".
		\sa		usbFilter, usbFilterType, usbClass, usbGUID
	*/
	/*!
		\var	bool	Conns::custom
				Flag indicate that this driver has a non standard comunnication method.
				The driver exports its custom port-list method List() that provides a packed
				string with all available ports.

				The deviceURL can be of any format and has only meaning to the driver itself.
		\sa		List
	*/
	/*!
		\var	bool	Conns::folder
				Flag indicate that this driver delivers a file or folder as output.

				The deviceURL needs to conform to the format "file://[full path]".
	*/
		
	//
	//		DriverInfo
	//
	/*!
		Driver information
		\ingroup	DRV_Info

		The Info function supplies the RIP with all needed information about the device like:
		-	Descriptive information like DPI, sizes and inks.
		-	Media sizes and trays
		-	Communication methods available for this device
		-	XML that generates the user interface for the device specific settings

		\sa	Info
	*/
	class DriverInfo
	{
	public:
		uint32_t		version;
		//
		//	Identification
		//
		char			name[140];
		char			profileName[32];
		uint32_t		uniqueDriverID;
		//
		//	Capabilities
		//
		Caps			caps;
		char			spectro[8];
		//
		//	Connection
		//
		Conns			conns;
		int16_t			serBaudRate;
		int16_t			serFlow;
		uint16_t		tcpPort;
#if ACPL_MAC || ACPL_LINUX
		char			usbFilter[32];
		USBFilterType	usbFilterType;
		uint8_t			usbClass;
#else
		char			parallelFilter[32];
		char			usbGUID[128];
#endif
		//
		//	Device capabilities
		//
		float			dpuOutputX;
		float			dpuOutputY;
		float			dpuRender;
		float			steps;
		float			maxWidth;
		float			maxHeight;
		MarkType		registrationType;
		char			markerGUID[40];
		float			sqUnitsPerHour;
		//
		//	Inks
		//
		int32_t			inkCount;
		TInk*			inks;
		char			inkType[64];
		//
		//	Media and trays
		//
		int32_t			mediaCount;
		int32_t			trayCount;
		TMediaSize*		media;
		TTray*			tray;
		//
		//	Custom settings script
		//
		char*			script;

						DriverInfo();
						~DriverInfo();

		DriverInfo&		operator>>(ACPL::Stream&);
		DriverInfo&		operator<<(ACPL::Stream&);
	private:
						DriverInfo( const DriverInfo& );
		DriverInfo&		operator=( const DriverInfo& );
	};

	/*!
		\var	char	DriverInfo::name[140]
				Describing the device name. E.g. "HP DesignJet 2000CP". This name corresponds with
				the name returned by ListModels(). The generic printer driver adds a driver prefix:
				"Generic " + 128 chr for the OS printer name
	*/
	/*!
		\var	char	DriverInfo::profileName[32]
				Defines the group name of models that can share the same printer profiles. This because
				the printers are only size variations or OEM models with a different name.
	*/
	/*!
		\var	uint32_t	DriverInfo::uniqueDriverID
				Unique driver ID used to facilitate licensing. This value correspond with the record
				number in the iOrder driver database and it used in the license product definition.
	*/
	/*!
		\var	char	DriverInfo::spectro[8]
				Build-in spectrophotometer for this printer. Empty if no spectophotometers is buildin. Currently supported:
				ILS20 (Epson), ILS30 (Epson), DTP20 (HP), Canon
	*/
	/*!
		\var	int16_t	DriverInfo::serBaudRate
				The default baudrate used by the device. Valid when the serial connection flag is set.
		\sa		Conns::serial, serFlow
	*/
	/*!
		\var	int16_t	DriverInfo::serFlow
				The default handshake used by the device. Valid when the serial connection flag is set.
				Valid values are:
				- 1 : No handshake
				- 2 : XOn / XOff
				- 3 : DTR / CTS
				- 4 : Both
		\sa		Conns::serial, serFlow
	*/
	/*!
		\var	int16_t	DriverInfo::tcpPort
				The default TCP port used by the device. Valid when the tcp connection flag is set.
				The port type also implictely defines the default protocol used:
				- 9100 : RTelnet / Raw
				- 515 : LPR/LPD
				- 80 : HTTP
		\sa		Conns::tcpip
	*/
	/*!
		\var	char	DriverInfo::usbFilter[32]
				MacOS X only : The USB IEEE or MfID filter for finding devices. Valid when the USB
				connection flag is set. The filter can be the exact MFG+MDL tag if the IEEE ID or
				it can be a partial name appended with an * wildcard. This filter will be used by
				the driver host to list suitable ports for this device.
		\sa		Conns::usb, usbFilterType, usbClass
	*/
	/*!
		\var	USBFilterType	DriverInfo::usbFilterType
				MacOS X only : The type of USB filter define by usbFilter. Valid when the USB
				connection flag is set. The filter can filter on the IEEE 1283 string or on the
				product field from the USBDescriptor.
		\sa		Conns::usb, usbFilter, usbClass
	*/
	/*!
		\var	uint8_t	DriverInfo::usbClass
				MacOS X only : The USB class of the device. Valid when the USB connection flag is set.
				If this value is set to 0 no filtering will be done on the USB device class.
		\sa		Conns::usb, usbFilter, usbFilterType
	*/
	/*!
		\var	char	DriverInfo::usbGUID[128]
				Windows only : The GUID of the driver that driver that drives this device in the OS.
				Valid when the USB connection flag is set.
		\sa		Conns::usb
	*/
	/*!
		\var	float	DriverInfo::dpuOutputX
				Horizontal output resolution of the image data the RIP must provide to the driver.
				The resolution is expressed in AOI Units which are 1/18th of an inch. This means
				that it is the DPI divided by 4.
		\sa		dpuOutputY, dpuRender
	*/
	/*!
		\var	float	DriverInfo::dpuOutputY
				Vertical output resolution of the image data the RIP must provide to the driver.
				The resolution is expressed in AOI Units which are 1/18th of an inch. This means
				that it is the DPI divided by 4.
		\sa		dpuOutputX, dpuRender
	*/
	/*!
		\var	float	DriverInfo::dpuRender
				Proposed render resolution of the contone data before screening. During the screening
				the data needs to be upsampled to dpuOutputX x dpuOutputY.
				The resolution is expressed in AOI Units which are 1/18th of an inch. This means
				that it is the DPI divided by 4.
		\sa		dpuOutputX, dpuOutputY
	*/
	/*!
		\var	float	DriverInfo::steps
				Number of steps per AOI Unit (1/18th of an inch) used by the plotter. All vector
				calls are to be provided in this resolution except for plotter that have the highlevel
				flag set. Valid only when the cut flag is set.
		\sa		Caps::highlevel
	*/
	/*!
		\var	float	DriverInfo::maxWidth
				Maximum imageable width of the device expressed in AOI units.
		\sa		maxHeight
	*/
	/*!
		\var	float	DriverInfo::maxHeight
				Maximum imageable height/length of the device expressed in AOI units.
		\sa		maxWidth
	*/
	/*!
		\var	float	DriverInfo::sqUnitsPerHour
				Amount of square units this devices produces using the given settings. Used for
				calculating production estimated. Valid only when the estimateInfo flag is set.
		\sa		Caps::estimateInfo
	*/
	/*!
		\var	MarkType	DriverInfo::registrationType
				Flags indicating which contour cutting registration marks are supported by this
				device.
	*/
	/*!
		\var	char	DriverInfo::markerGUID[40]
				If the registrationType is markCustom then this fields holds the GUID of the
	 			build-in marker template to be used.
	*/
	/*!
		\var	int32_t	DriverInfo::inkCount
				Number of ink channels that are available using the given settings. The
				ink definition array DriverInfo::inks is filled with this number of entries.
		\sa		inks
	*/
	/*!
		\var	TInk*	DriverInfo::inks
				Array of ink definitions. Every entry describes the ink of the corresponding
				head.
		\sa		inkCount
	*/
	/*!
		\var	char	DriverInfo::inks[64]
				Type of the ink used, like Solvent, UV-Curing.
	*/
	/*!
		\var	int32_t	DriverInfo::mediaCount
				Number of media sizes that are available using the given settings. The
				media definition array DriverInfo::media is filled with this number of entries.
		\sa		media
	*/
	/*!
		\var	TMediaSize*	DriverInfo::media
				Array of media definitions.
		\sa		mediaCount
	*/
	/*!
		\var	int32_t	DriverInfo::trayCount
				Number of trays that are available using the given settings. The
				tray definition array DriverInfo::tray is filled with this number of entries.
		\sa		tray
	*/
	/*!
		\var	TTray*	DriverInfo::tray
				Array of tray definitions.
		\sa		trayCount
	*/
	/*!
		\var	char*	DriverInfo::script
				XML based UI definition that describes the driver settings. An UI can be generated
				using this XML and results in the settings XML passed to the Info() and Initialize()
				calls. For more information about the XML based UI see http://developer.printfactory.cloud/driverui/
	*/

	/*!
		\class		ModelID
		\ingroup	DRV_Info
	*/
	typedef struct ModelID
	{
		const char*	name;
		const char*	profileName;
		uint32_t	uniqueDriverID;
		float		maxWidth;
		float		maxHeight;
	}	ModelID;

	extern const ModelID kDriverID[];
	/*!
		\var	const char*	ModelID::name
				Name of the device model
	*/
	/*!
		\var	const char*	ModelID::profileName
				Defines the group name of models that can share the same printer profiles. This because
				the printers are only size variations or OEM models with a different name.
	*/
	/*!
		\var	uint32_t	ModelID::uniqueDriverID
				Unique driver ID used to facilitate licensing a RIP with only 1 driver (model or
				group). When not used this value can be 0.
	*/
	/*!
		\var	float	ModelID::maxWidth
				Maximum imageable width of the device expressed in AOI units.
		\sa		maxHeight
	*/
	/*!
		\var	float	ModelID::maxHeight
				Maximum imageable height/length of the device expressed in AOI units.
		\sa		maxWidth
	*/

	/*!
		Halftone channel definition
		\ingroup	DRV_Info

		Definition of the parameters of a halftone channel, used when Screening is eAMScreen.
	*/
	class ChannelDefinition
	{
	public:
		char		name[64];	//!<	Name of the channel
		ScreenType	screening;	//!<Screening type of the channel
		float		Lab[3];		//!<	CIELab color that describes the color of the channel for preview purposes
		SpotType	shape;		//!<	Halftone spot shape
		float		angle;		//!<	Halftone angle
		float		frequency;	//!<	Halftone frequency in lines per inch
		bool		on;			//!<	Flag to indicate if this channel will be output or not
		
		inline ChannelDefinition() : screening( eAMScreen ), shape( eAdobeRound ), angle(0), frequency(52), on(true) { name[0] = 0; Lab[0] = 50; Lab[1] = Lab[2] = 0; }
	};

	/*!
		Halftone channel definition list
		\ingroup	DRV_Info

		Definition of the parameters of a halftone channel, used when Screening is eAMScreen. The list defines the channels used
		in the job and the order of rendering.
	*/
	class ChannelList : public std::vector<ChannelDefinition>
	{
	public:
		void	Load( const ACPL::XML& );
		void	Save( ACPL::XML& ) const;
		void	Add( const TInk& ink );
	};

	/*!
		Constructor of the DriverInfo class creates an empty info structure
		that can be used for the Info call.
	*/
	inline DriverInfo::DriverInfo() :
		version( SDK_VERSION ),
		uniqueDriverID( 0 ),
		inkCount( 0 ),
		inks( NULL ), 
		mediaCount( 0 ),
		trayCount( 0 ),
		media( NULL ),
		tray( NULL ),
		script( NULL )
	{
	}

	inline DriverInfo::~DriverInfo()
	{
		delete[] inks;
		delete[] media;
		delete[] tray;
		delete[] script;
	}

	/*!
		State returned from a Direct Access Method
		\ingroup	DRV_Methods
	*/
	typedef enum DirectState
	{
		ePlotterOK = 0,
		ePlotterBusy,
		ePlotterFunctionNotUsed,
		ePlotterError
	}	DirectState;

	class PageRect
	{
	public:
		float	left;
		float	top;
		float	right;
		float	bottom;

		inline	PageRect()
		{
			left = top = 3.402823466e+38F;
			right = bottom = -3.402823466e+38F;
		}

		inline	PageRect( float x1, float y1, float x2, float y2 )
		{
			left = x1;
			top = y1;
			right = x2;
			bottom = y2;
		}

		inline void	Union( const PageRect& inBounds )
		{
			if( inBounds.left < left )
				left = inBounds.left;
			if( inBounds.top < top )
				top = inBounds.top;
			if( inBounds.right > right )
				right = inBounds.right;
			if( inBounds.bottom > bottom )
				bottom = inBounds.bottom;
		}

		inline float Width() const
		{
			return right - left;
		}

		inline float Height() const
		{
			return bottom - top;
		}
	};

	class MarkerPosition
	{
	public:
		float	x;
		float	y;

		inline	MarkerPosition() : x(0), y(0) {}
		inline	MarkerPosition( float inX, float inY ) : x(inX), y(inY) {}
	};

	/*!
		Session methods
		\ingroup	DRV_Methods

		The session method exported by the driver. The entries might be NULL and are thus
		not supported. This means the call can ommitted and the next call in the calling order
		should be executed (see below).
	*/
	typedef struct DriverMethods
	{
		/*!
			Starts a session. Initialized the driver with the settings to be used. Many drivers will open
			the communication channel during this call.
			This the only method that will return an error. Other methods will throw a C++ exception of
			the type ExceptionCode.

			\param[in]	obj			Opaque driver reference
			\param[in]	jobDict		XML containing the job (ticket). This maybe be NULL. The driver can
									use information from the job and may write additional private or public data
									in the job.
			\param[in]	dpuX		Horizontal resolution of the halftone data that will be provided in AOI units.
			\param[in]	dpuY		Vertical resolution of the halftone data that will be provided in AOI units.
			\param[in]	settings	Settings XML containing the settings extracted from the XML based UI.
			\return					NoError when successfull. The error when the call failed. When the method
									ErrorToName is available the error must be translated using that method to
									get a user readeable error message.
		*/
		ExceptionCode	(*Initialize)( void* obj, ACPL::XML* jobDict, float dpuX, float dpuY, const char* settings );
		/*!
			Terminates/closes the session of the driver.

			\param[in]	obj			Opaque driver reference
			\param[in]	abort		True when the current connection and output needs to be aborted. False when
									a gracefull close and exit is requested.
		*/
		void			(*Terminate)( void* obj, bool abort );
		/*!
			Converts an error code to a readeable description. 

			\param[in]	err			The error to translate
			\param[out]	errorName	String of 256 characters long that will receive the description.
			\return					True when the error is known and a description is generated.
		*/
		bool			(*ErrorToName)( ExceptionCode err, char* errorName );
		/*!
			Adds a preview to the page that has just been closed. The BMP is expected to be in 24 bits RGB.

			\param[in]	obj			Opaque driver reference
			\param[in]	pathToBMP	Full path to the preview BMP that will be used as source of the preview.
		*/
		void			(*AddPreview)( void* obj, const ACPL::UString& pathToBMP );
		/*!
			Performs a direct home of the head without opening a job and page. The commands are directly
			flushed and thus performed. These commands are used to implement remote control of cutting
			heads for example.

			\param[in]	obj			Opaque driver reference
		*/
		DirectState		(*DirectHome)( void* obj );
		/*!
			Performs a direct move of the head without opening a job and page. The commands are directly
			flushed and thus performed. These commands are used to implement remote control of cutting
			heads for example.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in AOI units 
			\param[in]	y			Y coordinate in AOI units
		*/
		DirectState		(*DirectMove)( void* obj, float x, float y );
		/*!
			Sets the new home position of the head to the current position. The commands are directly
			flushed and thus performed. These commands are used to implement remote control of cutting
			heads for example.

			\param[in]	obj			Opaque driver reference
		*/
		DirectState		(*DirectSetHome)( void* obj );
		/*!
			Queries the size of the currently loaded media. The commands are directly flushed and
			thus performed. There is no need to open a job or a page. If the device uses a different 
			protocol or returns more than only the size then use the exported DLL method PrinterStatus.

			\param[in]	obj			Opaque driver reference
			\param[out]	outSize		Rectangle that receives the media size.
		*/
		DirectState		(*DirectSizeQuery)( void* obj, PageRect& outSize );
		/*!
			Queries the current position of the head. The commands are directly flushed and
			thus performed. There is no need to open a job or a page.

			\param[in]	obj			Opaque driver reference
			\param[out]	outCoord	Coordinate that receives the head position.
		*/
		DirectState		(*DirectPosQuery)( void* obj, float& outCoordX, float& outCoordY );
		void			(*ObsoleteMethod)();
		DirectState		(*DirectBarcodeQuery)( void*, ACPL::String& );
		/*!
			Start a job on the device. A job can contain more than 1 pages. This call must be paired with
			JobEnd after all pages are send.

			\param[in]	obj				Opaque driver reference
			\param[in]	jobName			Name of the job
			\param[in]	copies			Number of copies. When the device has internal copy facilities the copies should
										be set to 0 before returning.
			\param[in]	mediaSize		Media size to be used. The media size must be respecting the margins and sizes set by the tray.
			\param[in]	tray			Tray to be used. This needs to be a copy of a TTray entry returned bij DriverInfo.
			\param[in]	hasImageData	Flag that indicates if the job contains image data (a print)
			\param[in]	hasVectorData	Flag that indicates if the job contains vector data (contour cut)
			\sa			JobEnd
		*/
		void			(*JobStart)( void* obj, const ACPL::UString& jobName, int32_t& copies, const TMediaSize& mediaSize, const TTray& tray, bool hasImageData, bool hasVectorData );
		/*!
			Ends the job on the device. Can only be called after all pages are send and a job is open.

			\param[in]	obj			Opaque driver reference
			\sa			JobStart
		*/
		void			(*JobEnd)( void* obj );
		/*!
			Passed the list of registration mark positions if present.

			\param[in]	obj			Opaque driver reference
			\param[in]	nrOfPoints	Number of registration points. The array points is expected to contain nrOfPoints values
			\param[in]	points		Array of MarkerPositions in AOI units, containing nrOfPoints elements.
		*/
		void			(*PageRegistrationMarks)( void* obj, uint32_t nrOfPoints, const MarkerPosition * const points );
		/*!
			Start a page on the device. This call must be paired with PageEnd. A call to JobStart must
			preceed this call.

			\param[in]	obj			Opaque driver reference
			\param[in]	pageNr		1 based page number, sides are not counted as additional pages
			\param[in]	side		Page side indicator:
									- S : Single sided page, next page is a new page
									- A : Front side, next page is a side of the same page is X or B
									- X : Blocker, next page is B (side)
									- B : Back side, last side of the page, can also be the first side if no A side exists
			\param[in]	barcode		Barcode identifying this page. When no barcode is provided then this is empty. The barcode is
									identical for the printed page and the cut page that is associated.
			\param[in]	mediaSize	Media size to be used. The media size must be respecting the margins and sizes set by the tray.
			\param[in]	tray		Tray to be used. This needs to be a copy of a TTray entry returned bij DriverInfo.
			\param[in]	imagedRect	Area of the page filled by the data to come expressed on AOI units where the
									origin is the top left corner of the imageable area.
			\param[in]	metaPath	Path to the META info XML of the page being started.
			\sa			PageEnd
		*/
		void			(*PageStart)( void* obj, uint32_t pageNr, char side, const ACPL::String& barcode, const TMediaSize& mediaSize, const TTray& tray, const PageRect& imagedRect, const ACPL::UString& metaPath );
		/*!
			Ends the page, a page must be open.

			\param[in]	obj			Opaque driver reference
			\param[in]	endOffset	Location where the (cutting) head must go when the page is finished:
									- 1 : Move the head to the origin of the impression
									- 2 : Move to the bottom left of the impression
									- 3 : Move to the right top of the impression
									- 4 : Transport media (flatbed) by the length of the impression
									- 5 : Same as 4 and cut the media
									- 6 : Manually position the head (device goes offline)
			\return					True when the same page needs to be send again. This is used for layered
									printing and as long as not all layers are ready true is returned. Return
									false to proceed to the next page.
			\sa			PageStart
		*/
		bool			(*PageEnd)( void* obj, int16_t endOffset );
		/*!
			Invokes the roll cutter of the printer.

			\param[in]	obj			Opaque driver reference
		*/
		void			(*PanelCut)( void* obj );
		//
		//	Rasterizing
		//
		/*!
			Starts a print on the device. Only one call to RasterStart is allowed per page.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			Left offset in pixels relative to the imageble area of the printer.
			\param[in]	y			Top offset in pixels relative to the imageble area of the printer.
			\param[in]	width		Width of the print in pixels.
			\param[in]	height		Height of the print in pixels.
			\sa			RasterEnd, ProcessRawBuffer
		*/
		void			(*RasterStart)( void* obj, int32_t x, int32_t y, int32_t width, int32_t height );
		/*!
			Ends a print on the device.

			\param[in]	obj			Opaque driver reference
			\sa			RasterStart, ProcessRawBuffer
		*/
		void			(*RasterEnd)( void* obj );
		/*!
			Processes 1 plane of a line of the image. The driver expect all planes of a line even if they
			are empty, so for a CMYK print 4 calls to this method are required to process 1 line.
			The provided halftone data must be truncated so that no white exists on the right side of the
			plane. When this is not done the printing efficiency might go down. It is allow to pass a NULL
			pointer when the data length is 0.

			\param[in]	obj			Opaque driver reference
			\param[in]	planeData	Pointer to the halftoned data of 1 plane.
			\param[in]	dataBytes	Number of bytes of halftone data.
			\param[in]	planeIdx	Channel number to which this plane data refers
			\sa			RasterStart, RasterEnd
		*/
		void			(*ProcessRawBuffer)( void* obj, const uint8_t* planeData, int32_t dataBytes, uint8_t planeIdx );
		//
		//	Cutting
		//
		/*!
			Starts a plot on the device. A plot can be drawing, cutting, routing or engraving. Only one
			call to VectorStart is allowed per page.

			\param[in]	obj			Opaque driver reference
			\sa			VectorEnd
		*/
		void			(*VectorStart)( void* obj );
		/*!
			Ends a plot on the device.

			\param[in]	obj			Opaque driver reference
			\sa			VectorStart
		*/
		void			(*VectorEnd)( void* obj );
		/*!
			Moves the current point of the plot to the given coordinate.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in DriverInfo::steps 
			\param[in]	y			Y coordinate in DriverInfo::steps
		*/
		void			(*Move)( void* obj, int32_t x, int32_t y );
		/*!
			Draws a line from the current point of the plot to the given coordinate and makes it the
			current point.

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in DriverInfo::steps 
			\param[in]	y			Y coordinate in DriverInfo::steps
		*/
		void			(*Line)( void* obj, int32_t x, int32_t y );
		/*!
			Draws a line from the current point of the plot to the given coordinate and makes it the
			current point. This line segment is originated from a curve (Bezier, B-spline or other)

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in DriverInfo::steps 
			\param[in]	y			Y coordinate in DriverInfo::steps
			\param[in]	last		True when this is the last line segment of the curve.
		*/
		void			(*Vertex)( void* obj, int32_t x, int32_t y, bool last );
		/*!
			Instructs the plotter to raise the tool/pen.

			\param[in]	obj			Opaque driver reference
			\sa			CurveStart, CurveEnd
		*/
		void			(*PenUp)( void* obj );
		/*!
			Instructs the plotter to lower the tool/pen.

			\param[in]	obj			Opaque driver reference
		*/
		void			(*PenDown)( void* obj );
		/*!
			Marks the start of a series of line segments forming a curve.

			\param[in]	obj			Opaque driver reference
			\sa			Vertex, CurveEnd
		*/
		void			(*CurveStart)( void* obj );
		/*!
			Marks the end of a series of line segments forming a curve.

			\param[in]	obj			Opaque driver reference
			\sa			Vertex, CurveStart
		*/
		void			(*CurveEnd)( void* obj );
		/*!
			Instructs the plotter to select the specified tool as defined by
			the passed driver setting.

			\param[in]	obj			Opaque driver reference
			\param[in]	settings	Settings XML containing the settings extracted from the XML based UI
									for the tool to be selected.
		*/
		void			(*SetTool)( void* obj, const char* settings );
		//
		//	High level
		//
		/*!
			Moves the current point of the plot to the given coordinate.

			Only valid when Caps::highlevel flag is set

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in AOI units 
			\param[in]	y			Y coordinate in AOI units
			\param[in]	closed		True when the path that is about to start is closed, false if it is open
		*/
		void			(*HLMove)( void* obj, float x, float y, bool closed );
		/*!
			Draws a line from the current point of the plot to the given coordinate and makes it the
			current point.

			Only valid when Caps::highlevel flag is set

			\param[in]	obj			Opaque driver reference
			\param[in]	x			X coordinate in AOI units 
			\param[in]	y			Y coordinate in AOI units
		*/
		void			(*HLLine)( void* obj, float x, float y );
		/*!
			Draws a bezier from the current point of the plot and makes x3,y3 the
			current point.

			Only valid when Caps::highlevel flag is set

			\param	obj			Opaque driver reference
			\param	x1			X coordinate in AOI units of the first Bezier handle
			\param	y1			Y coordinate in AOI units of the first Bezier handle
			\param	x2			X coordinate in AOI units of the second Bezier handle
			\param	y2			Y coordinate in AOI units of the second Bezier handle
			\param	x3			X coordinate in AOI units of the Bezier end point
			\param	y3			Y coordinate in AOI units of the Bezier end point
		*/
		void			(*HLBezier)( void* obj, float x1, float y1, float x2, float y2, float x3, float y3 );
		/*!
			Obsolete function, kept here as placeholder
		 */
		void			(*CustomAction)( void* obj, int a, const char* b );
		/*!
			Start a job on the device. A job can contain more than 1 pages. This call must be paired with
			JobEnd() after all pages are sent with PageAvailable().

			\param[in]	obj				Opaque driver reference
			\param[in]	jobParts		The container with all the job parts that will be further printed individually.
										This is needed by the driver to reconstruct the job layout.
			\param[in]	requestID		The ID of the request that comes from the device. This will be re-sent by the driver
										to the device alongside the processed information.
			\param[in]	jobName			Name of the job
			\sa			JobEnd
		*/
		void			(*StartOnDemandJob)( void* obj, Imposition* jobParts, const ACPL::String& requestID, const ACPL::UString& jobName );
		/*!
			When this method is available the driver is able to internally repeat the raster vertically.
			If this method is not available a repeat of 1 is assumed.

			\param[in]	obj				Opaque driver reference
			\param[in]	repeats			The number of repeats of the raster area vertically.
										The RasterStart width/height parameters define the size of the first tile.
			\return						True when the repeats are accepted. Return false to invoke in RIP repeating.
			\sa			RasterStart
		*/
		bool			(*RasterRepeat)( void* obj, uint32_t repeats );
	}	DriverMethods;

	static const ExceptionCode	kErrorLookup = 0x4C4B4552;	//	'LKER'
	static const ExceptionCode	kErrorRefused = 0x52454655;	//	'REFU'

	class	Instance;
		
	/*!
		Instance methods
		\ingroup	INST_Methods

		The instance method exported by the driver. The entries might be NULL and are thus
		not supported. This means the call can ommitted and the next call in the calling order
		should be executed (see below).
	*/
	typedef struct InstanceMethods
	{
		/*!
			The Info function supplies the RIP with all needed information about the device like:
			-	Descriptive information like DPI, sizes and inks.
			-	Media sizes and trays
			-	Communication methods available for this device
			-	An XML that generates the user interface for the device specific settings

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	info		Initialized info class that will receive the driver
			\param	settings	XML containing the driver settings. When an empty string is supplied the
								default settings are used to fill the DriverInfo
		*/
		ExceptionCode	(*Info)( Instance* driverObj, DriverInfo* info, const char* settings );
		/*!
			\ingroup	DLL_API

			The DisposeInfo function deletes the content of the DriverInfo object allocated by the Info-call after which the
			calling host can dispose the DriverInfo object.

			\param	info		DriverInfo object to dispose
			\sa	Info, DriverInfo
		*/
		void			(*DisposeInfo)( DriverInfo* info );
		/*!
			This function is called to create an instance of the driver for the given model. This function is
			not called when information about the driver is queried (like media sizes) only when a job has to
			be processed.

			The CreateSession function supplies the RIP with a reference to an object. This reference is opaque to the RIP.
			This means that the RIP does not know the definition of the object. The RIP uses this reference only as
			a reference of the driver and has only a meaning to the driver it self (e.g. it might be the address of
			a C++ object or a structure in memory to hold information needed during processing)
			The Create function also supplies a pointer to a table of routine pointers. Those routines are called
			during the processing of the job. Every routine has as first parameter the aforementioned reference.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	methods		Pointer that receives the pointer to the DriverMethods table of the driver.
			\param	deviceURL	URL that describes the connection to be used. When NULL is specified then the default URI
								will be used. Only used for folder based devices.
			\return				Opaque driver reference to be used with all driver methods and Dispose.
			\sa	DisposeSession, DriverMethods
		*/
		void*			(*CreateSession)( Instance* driverObj, const DriverMethods** methods, const char* deviceURL );
		/*!
			This function is called when the RIP is done using the instance of the driver after processing a job.

			\param	obj			Opaque driver reference returned by Create().
			\sa	CreateSession
		*/
		void			(*DisposeSession)( Instance* driverObj, void* obj );
		/*!
			PrinterStatus returns information about the printer ink levels and loaded media if the routine is available
			and the printer supports this information.
			The data is returned in XML form containing all the information available, if the printer is not supporting a
			certain type of feedback the XML will not contain the keywords.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	obj			Opaque driver reference returned by Create().
			\param statusDict	XML DOM containing the status information on return, documentation of the keyword can be found in the xds
		 */
		void			(*PrinterStatus)( Instance* driverObj, ACPL::XML* statusDict );
		/*!
			JobStatus returns information about a job if the routine is available and the printer supports this information.
			The data is returned in XML form containing all the information available, if the printer is not supporting a
			certain type of feedback the XML will not contain the keywords.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	jobGUID     GUID of the job send to the printer.
			\param	statusDict	XML DOM containing the job information on return, documentation of the keyword can be found in the xds
		 */
		void			(*JobStatus)( Instance* driverObj, const char* jobGUID, ACPL::XML* statusDict );
		/*!
			GetPrivateCacheData retrieves information stored by the driver in its private cache to allow it to be retained. This
			private data can be cached information from the actual printer to speed-up the queries or to build the proper content
			of the DriverInfo structure (like medias installed at the printer).

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	outStream   Empty stream that receives that private cache data that is used to initialize this printer instance.
			\sa		SetPrivateCacheData
		 */
		void			(*GetPrivateCacheData)( Instance* driverObj, ACPL::Stream& outStream );
		/*!
			SetPrivateCacheData stored retained private information used by the driver to overwrite its private cache. This
			private data can be cached information from the actual printer to speed-up the queries or to build the proper content
			of the DriverInfo structure (like medias installed at the printer).

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	inStream	Stream with its marker set to the start of the stream and contain the private cache data that is
								used to initialize this printer instance. The data is obtained earlier using GetPrivateCacheData.
			\sa		GetPrivateCacheData
		 */
		void			(*SetPrivateCacheData)( Instance* driverObj, ACPL::Stream& inStream );
		/*!
			SetHostInterface provides a callback mechanism for the driver to call specific functions in the hosting application.
			The call is generalized in an XML query and XML response, the provided functionality through this API is host specific
			and can't be trusted to be available in every host.

			\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param	hostName	Name to identify the host, this can be used by the driver to determine which API is available.
			\param	hostObj		Reference to the host object, this needs to be provided by every call to the host-callback routine.
			\param	hostCB		Routine pointer providing query-XML as input and response-XML as output XML. Return value true means
								success and false that the API is not available or the call failed.
		 */
		void			(*SetHostInterface)( Instance* driverObj, const char* hostName, void* hostObj, bool (*hostCB)( void* hostObj, ACPL::XML& query, ACPL::XML& response ) );
		/*!
			Query used to by pass the workflow component. Used by the Calibrator/Profiler to access build-in
			photospectrometers. The implementation is device dependend, the API is generalization of
			the process.

			\param		driverObj	Reference to the printer object obtained from PrinterIntitialize.
			\param[in]	query		Query to the device.
			\param[out]	response	Response from the device.
		*/
		bool			(*PrinterQuery)( Instance* driverObj, ACPL::XML& query, ACPL::XML& response );
	}	InstanceMethods;

	/*!
		\ingroup	DLL_API

		The ListModels function supplies the PrintFactory RIP with al list of all models supported by this driver.

		\return	Static list of models. The last entry in the model list is a NULL pointer to mark the end of the list.
	*/
	DLLAPI	const ModelID*	CALLBACK	ListModels();
	/*!
		\ingroup	DLL_API

		PrinterInitialize is to be called once at the start-up of the host(RIP) when the routine is available.
		The routine initializes the printer once, like updating cache files, downloading media definitions, etc.

		\param[in]	modelName	Name of the printer/plotter model as supplied by ListModels().
		\param[in]	deviceURL	URL that describes the connection to be used. Meaning full only to the driver.
		\param[out]	methods		List of routine pointers that can be called to process the job
		\return					Reference to a printer object, this is passed to Create and PrinterTerminate objects.
	*/
	DLLAPI	Instance*		CALLBACK	PrinterInitialize( const char* modelName, const char* deviceURL, const InstanceMethods** methods );

	typedef bool (*DriverInitCB)( void* hostObj, ACPL::XML& status );

	/*!
		\ingroup	DLL_API

		PrinterUpdateDriver is to be called directly after PrinterInitialize to update modes, definitions or complete drivers.
		This is an option method.

		\param[in]	driverObj	Reference to the printer object obtained from PrinterIntitialize.
		\param[in]	proxy		URL to the proxy to use for http communication. If not specified (NULL or empty string) then
								direct internet access is assumed.
		\param[in]	region		Region code: 0 = international, 1 = PRC China
		\param[in]	hostObj		Reference to the host object, this needs to be provided by every call to the host-callback routine.
		\param[in]	progressCB	Routine pointer providing status XML to the host reporting on the init progress. Return value true
								means success and false that the initialization is to be aborted.
		\return					True if the update has been executed succesfully.
	*/
	DLLAPI	bool			CALLBACK	PrinterUpdateDriver( Instance* driverObj, const char* proxy, uint8_t region, void* hostObj, DriverInitCB progressCB );
	/*!
		\ingroup	DLL_API

		PrinterInitialize is to be called when the host(RIP) closes if the routine is available.

		\param	driverObj	Reference to the printer object obtained from PrinterIntitialize.
	*/
	DLLAPI	void			CALLBACK	PrinterTerminate( Instance* driverObj );
	/*!
		\ingroup	DLL_API

		The ListChannels function supplies the PrintFactory RIP with al list of all custom connection ports. The result is a packed string
		list, separated by 0 and terminated by an empty string. This method is to be called when custom is used as connection method
		and the chosen connection name must be passed as connection URI without additional resource selector (as-is)
	 
		\param[in]	modelName	Name of the model from which the custom connection list is to be queried.
		\param[out]	list		Pack-string list containing all custom connection end-points, terminated by an empty string.
								The string passed should be at least 1024 bytes to hold all strings.
	*/
	DLLAPI	void			CALLBACK	ListChannels( const char* modelName, char* list );
}}

